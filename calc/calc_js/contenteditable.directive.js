angular.module('calc')
    .directive('contenteditable', contenteditable);

contenteditable.$inject = [];

function contenteditable() {
    return {
        restrict: 'A',
        require: "ngModel",
        link: link
    };

    function link(scope, element, attrs, ngModel) {

        function read() {
            ngModel.$setViewValue(element.text());
        }

        ngModel.$render = function() {
            element.text(ngModel.$viewValue || "");
        };

        element.bind("blur keyup change", function() {
            scope.$apply(read);
        });
    }
}
