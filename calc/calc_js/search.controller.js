angular.module('calc')
    .controller('searchCtrl', searchController);

searchController.$inject = ['$rootScope', '$http', '$location', '$routeParams', 'commonService', 'currentPricesService',
    'sectionsUpgraderService', 'usedFactorsService'];

function searchController($rootScope, $http, $location, $routeParams, common, currentPrices, sectionsUpgrader,
                          usedFactorsService) {
    $rootScope.isPrintPageOpen = $routeParams.print === 'true';
    $rootScope.calcId = $routeParams.id;
    $rootScope.cloneSections = [];

    // $http.get('https://kubdem.ru/calc/item?id=' + $rootScope.calcId)
    $http.get('/mock/item_id=' + $rootScope.calcId + '.json')
        .then(function (responce) {
            var data = responce.data;

            if (data && data.item) {
                if (data.item.priceId) {
                    currentPrices.loadById(data.item.priceId, data.item)
                        .then(function () {
                            setData(data);
                        });
                } else {
                    currentPrices.loadFromOptions(data.item.options, data.item);
                    setData(data);
                }
            } else {
                clearDataAndToHome();
            }
        }, function () {
            clearDataAndToHome();
        });

    function clearDataAndToHome() {
        common.clearRootScope();
        currentPrices.clear();
        usedFactorsService.clear();
    }

    function setData(data) {
        $rootScope.sections = sectionsUpgrader.upgradeSections(data.item.sections);
        $rootScope.sections.clientDiscount = data.item.clientDiscount || 0;
        $rootScope.sections.isClientDiscount = !!$rootScope.sections.clientDiscount;
        $rootScope.nds = data.item.nds;
        $rootScope.dateCreate = data.date_create;
        $rootScope.cloneSections = angular.copy($rootScope.sections);
        $rootScope.cloneSections.clientDiscount = $rootScope.sections.clientDiscount;
        $rootScope.cloneSections.isClientDiscount = $rootScope.sections.isClientDiscount;
        $rootScope.isEditSections = false;
        $rootScope.isPrint = false;

        common.itogo();

        if ($rootScope.isPrintPageOpen) {
            $location.path('/print');
        } else {
            $location.path('/');
        }
    }
}
