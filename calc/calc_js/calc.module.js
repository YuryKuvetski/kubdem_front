angular.module('calc', ['ngRoute', 'ui.bootstrap'])
    .config(calcModuleConfig);

calcModuleConfig.$inject = ['$routeProvider'];

function calcModuleConfig($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: '/calc_templates/calc_index.html',
        controller: 'indexCtrl'
    }).when('/print', {
        templateUrl: '/calc_templates/calc_print.html',
        controller: 'printCtrl'
    }).when('/addSection', {
        templateUrl: '/calc_templates/calc_addSection.html',
        controller: 'addSectionCtrl'
    }).when('/addItem', {
        templateUrl: '/calc_templates/calc_addItem.html',
        controller: 'addItemCtrl'
    }).when('/item/:id/print/:print', {
        templateUrl: '/calc_templates/calc_index.html',
        controller: 'searchCtrl'
    }).otherwise({
        redirectTo: '/'
    });
}
