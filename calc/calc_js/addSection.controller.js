angular.module('calc')
    .controller('addSectionCtrl', addSectionController);

addSectionController.$inject = ['$rootScope', '$scope', '$location', 'commonService', '$http'];

function addSectionController($rootScope, $scope, $location, common, $http) {
    if (!$rootScope.sections) {
        $location.path('/');
        return;
    }

    var currentSection = $rootScope.sections[$rootScope.curentSection];

    $scope.addSection = addSection;
    $scope.pushSection = pushSection;
    $scope.removeSection = removeSection;
    $scope.exit = exit;

    $rootScope.isSearchBarVisible = false;

    $scope.isUserCanEdit = window.isUserCanEdit;
    $scope.input = [];
    $scope.dropdownButtons = [
        {
            class: 'glyphicon glyphicon-menu-up selectable-td vertical-align_middle text-success',
            isVisible: function (value, index) {
                return !!index;
            },
            onClick: moveSectionTop
        },
        {
            class: 'glyphicon glyphicon-menu-down selectable-td vertical-align_middle text-success',
            isVisible: function (value, index) {
                return index !== ($scope.nameDropdownData.length - 1);
            },
            onClick: moveSectionDown
        },
        {
            class: 'glyphicon glyphicon-remove selectable-td vertical-align_middle text-danger',
            isVisible: function () {
                return true;
            },
            onClick: removeSection
        }
    ];

    if ($rootScope.curentSection === false) {
        $scope.input['title'] = 'Наименование раздела';
    } else {

        $scope.input['title'] = currentSection.name;
        $scope.input['name'] = currentSection.name;
        $scope.input['comment'] = currentSection.comment;
    }

    $scope.nameDropdownData = [];
    getSections();

    function addSection() {
        if (!$scope.input['name']) {
            return;
        }

        if ($rootScope.curentSection === false) {
            $rootScope.sections.push({
                'name': $scope.input['name'],
                'comment': $scope.input['comment'],
                'items': [],
                'itogo': 0
            });
        }

        if ($rootScope.curentSection !== false) {
            $rootScope.sections[$rootScope.curentSection]['name'] = $scope.input['name'];
            $rootScope.sections[$rootScope.curentSection]['comment'] = $scope.input['comment'];
            $rootScope.curentSection = false;
        }

        $rootScope.isEditSections = common.isEditSections();
        $location.path('/');
    }

    function exit() {
        $location.path('/');
    }

    function pushSection() {
        var value = $scope.input['name'];

        if (!value) {
            return;
        }

        for (var i = 0; i < $scope.nameDropdownData.length; i++) {
            if ($scope.nameDropdownData[i].name === value) {
                return;
            }
        }

        $scope.nameDropdownData.push({name: $scope.input['name']});
        saveSections();
    }

    function removeSection(value, index) {
        if (confirm('Удалить "' + value + '"?')) {
            $scope.nameDropdownData.splice(index, 1);
            saveSections();
        }
    }

    function moveSectionTop(value, index) {
        common.swapInTheArray($scope.nameDropdownData, index, index - 1);
        saveSections();
    }

    function moveSectionDown(value, index) {
        common.swapInTheArray($scope.nameDropdownData, index, index + 1);
        saveSections();
    }

    function getSections() {
        // TODO Local
        $http.get('/mock/sections.json')
        // TODO Release
        // $http.get('https://kubdem.ru/calcdev/sections')
            .then(function (responce) {
                if (responce.data && responce.data.length) {
                    $scope.nameDropdownData = responce.data.map(function (t) {
                        return t.item || t
                    });
                }
            });
    }

    function saveSections() {
        // TODO Release
        // $http.post('https://kubdem.ru/calcdev/savesections', {items: $scope.nameDropdownData})
        //     .then(function () {
        //     });
    }
}
