angular.module('calc')
    .directive('dropdownList', dropdownList);

dropdownList.$inject = ['$timeout'];

function dropdownList($timeout) {
    return {
        restrict: 'E',
        scope: {
            itemsList: '=',
            searchResult: '=',
            buttons: '=',
            withButtons: '<',
            inputClass: '@',
            title: '@',
            placeholder: '@',
            onChange: '&',
            onBlur: '&'
        },
        templateUrl: 'calc_templates/dropdownList.template.html',
        link: link
    };

    function link(scope, el) {
        var $listContainer = angular.element(el[0].querySelectorAll('.search-item-list')[0]);

        scope.search = scope.searchResult || '';

        el.find('input').bind('focus', function () {
            $listContainer.addClass('show');
        });

        el.find('input').bind('blur', function () {
            $timeout(function () {
                $listContainer.removeClass('show');
            }, 200);

            if (scope.onBlur){
                scope.onBlur();
            }
        });

        scope.choose = function () {
            scope.searchResult = scope.search;

            if (scope.onChange){
                scope.onChange();
            }
        };

        scope.buttonClick = function (event, item, button, index) {
            event.preventDefault();
            event.stopImmediatePropagation();
            button.onClick(item.name, index);
        };

        scope.chooseItem = function (item) {
            scope.search = item.name;
            scope.searchResult = scope.search;
            $listContainer.removeClass('show');
        };
    }
}
