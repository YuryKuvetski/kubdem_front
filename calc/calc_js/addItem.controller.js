angular.module('calc')
    .controller('addItemCtrl', addItemController);

addItemController.$inject = ['$rootScope', '$scope', '$location', 'commonService', 'currentPricesService'];

function addItemController($rootScope, $scope, $location, common, currentPrices) {
    var options = currentPrices.options;
    var commonCheckboxData = null;

    if (!$rootScope.sections) {
        $location.path('/');
        return;
    }

    $rootScope.isSearchBarVisible = false;

    // Сервисы
    $scope.common = common;
    $scope.currentPrices = currentPrices;
    $scope.selectingMaterialId = null;
    $scope.materialsForMaterialSelector = options.materials;
    $scope.selectedMaterials = {
        sideA: null,
        construct: null,
        sideB: null
    };
    $scope.multilayer = {
        isMultilayer: false,
        multilayerText: ''
    };

    // Методы
    $scope.addItem = addItem;
    $scope.sumItogo = sumItogo;
    $scope.exit = exit;
    $scope.isSaveButtonDisabled = isSaveButtonDisabled;
    $scope.onRegionSelected = onRegionSelected;
    $scope.onAddSectionSelected = onAddSectionSelected;
    $scope.getClassForFactorsColumn = getClassForFactorsColumn;
    $scope.isColumnVisible = isColumnVisible;
    $scope.numForFirstColumn = numForFirstColumn;
    $scope.numForSecondColumn = numForSecondColumn;
    $scope.numForThirdColumn = numForThirdColumn;
    $scope.onMaterialSelected = onMaterialSelected;
    $scope.startSelect = startSelect;
    $scope.getMaterialDescription = getMaterialDescription;
    $scope.clearSelectedMaterials = clearSelectedMaterials;
    $scope.onClearSelectedMaterial = onClearSelectedMaterial;
    $scope.toggleCheckboxTab = toggleCheckboxTab;
    $scope.onDiscountChange = onDiscountChange;
    $scope.onMinSumChange = onMinSumChange;
    $scope.onWorkTypeChange = onWorkTypeChange;
    $scope.isSelectedMaterialButtonVisible = isSelectedMaterialButtonVisible;
    $scope.getFactureChunks = getFactureChunks;
    $scope.moveLayerLeft = moveLayerLeft;
    $scope.moveLayerRight = moveLayerRight;

    $scope.options = options;
    $scope.input = [];
    $scope.checkbox = {};
    $scope.currentCheckboxTab = null;
    $scope.availableAparatRezki = options.aparatRezki;
    $scope.factureChunks = [];

    $scope.input['work'] = options.work_types[0];
    $scope.input['work_2'] = $scope.availableAparatRezki[0];
    $scope.input['kolvo'] = 1;
    $scope.input['selectedMaterials'] = $scope.selectedMaterials;
    $scope.input['material_2'] = options.materialRezki[0];
    $scope.input['diameter'] = options.diameters[0];
    $scope.input['glubina_3'] = 20;
    $scope.input['material_3'] = 'Армированный бетон';
    $scope.input['name_3'] = 'Монтаж химического анкера';
    $scope.input['edizm_3'] = 0;
    $scope.input['edizmStr_3'] = 'шт.';
    $scope.input['edizmPrice_3'] = 0;
    $scope.input.discount = null;
    $scope.input.minSum = 0;
    // Для транспортных услуг
    $scope.input['region'] = options.regions
        && options.regions[0];
    $scope.input['place'] = options.regions
        && options.regions[0]
        && options.regions[0].places[0];
    // Для дополнительных услуг
    if (options.addSections) {
        $scope.input['addSection'] = options.addSections[0];
        $scope.input['addProduct'] = options.addSections[0] && options.addSections[0].products[0];
    }


    if ($rootScope.editItemTrue !== false) {
        var el = $rootScope.sections[$rootScope.curentSection]['items'][$rootScope.editItemTrue];

        $scope.input['work'] = common.getById(options.work_types, el['work']);
        $scope.onWorkTypeChange();

        // Если сверлим
        if ($scope.input['work']['id'] === 1) {
            $scope.input['itemName'] = el['itemName'];
            $scope.input['plos'] = el['plos'];
            $scope.input['comment'] = el['comment'];
            $scope.multilayer = el.multilayer || $scope.multilayer;
            $scope.input['selectedMaterials'] = el['selectedMaterials'] || $scope.selectedMaterials;
            $scope.selectedMaterials = $scope.input['selectedMaterials'];
            $scope.input['diameter'] = currentPrices.getDiameterByName(el['diameterName']);
            $scope.input['glubina'] = el['glubina'];
            $scope.input['kolvo'] = el['kolvo'];
            $scope.currentCheckboxTab = getFirstSelectedMaterialKey();
            $scope.input.discount = getDiscountValueForInput();
            $scope.input.minSum = el.minSum || 0;
            $scope.checkbox = $scope.currentCheckboxTab
                ? $scope.selectedMaterials[$scope.currentCheckboxTab].checkbox
                : el['vars'] && el['vars']['checkbox'] || {};
            changeCheckBoxArrayToObject();
            $scope.factureChunks = getFactureChunks();
        }

        // Если режем
        if ($scope.input['work']['id'] === 2) {
            $scope.input['itemName'] = el['itemName'];
            $scope.input['work_2'] = common.getById(options.aparatRezki, el['work_2']);
            $scope.input['plos'] = el['plos'];
            $scope.input['comment'] = el['comment'];
            $scope.multilayer = el.multilayer || $scope.multilayer;
            $scope.input['selectedMaterials'] = el['selectedMaterials'] || $scope.selectedMaterials;
            $scope.selectedMaterials = $scope.input['selectedMaterials'];
            $scope.input['glubina'] = el['glubina'];
            $scope.input['kolvo'] = el['kolvo'] * 100000;
            $scope.currentCheckboxTab = getFirstSelectedMaterialKey();
            $scope.input.discount = getDiscountValueForInput();
            $scope.input.minSum = el.minSum || 0;
            $scope.checkbox = $scope.currentCheckboxTab
                ? $scope.selectedMaterials[$scope.currentCheckboxTab].checkbox
                : el['vars'] && el['vars']['checkbox'] || {};
            changeCheckBoxArrayToObject();
            $scope.factureChunks = getFactureChunks();
        }

        // Если дополнительная услуга
        if ($scope.input['work']['id'] === 3) {
            $scope.input['name_3'] = el['comment'];
            $scope.input['material_3'] = el['material'];
            $scope.input['glubina_3'] = el['glubina'];
            $scope.input['kolvo'] = el['kolvo'];
            $scope.input['edizm_3'] = el['edizm_3'];
            $scope.input['edizmStr_3'] = el['edizmStr_3'];
            $scope.input['edizmPrice_3'] = el['sumItem'];

            if ($scope.input['addSection'] && $scope.input['addProduct']) {
                $scope.input['addSection'] = currentPrices.getAddSectionByName(el.addSectionName);
                $scope.input['addProduct'] =
                    currentPrices.getAddProductByName($scope.input['addSection'].products, el.addProductName);
            }
        }

        // Если транспортные расходы
        if ($scope.input['work']['id'] === 4) {
            $scope.input['region'] = currentPrices.getRegionByName(el.regionName);
            $scope.input['place'] = currentPrices.getPlaceByName($scope.input['region'].places, el.placeName);
            $scope.input['kolvo'] = el['kolvo'];
            $scope.input['comment_4'] = el['comment'];
        }
    }

    function addItem() {
        // TODO: Из-за проблем реализации выбора чекбоксов нужно сгенерить 1 выбор сейчас, чтобы все применилось!
        toggleCheckboxTab($scope.currentCheckboxTab);

        var push = {};
        var sumItemValue = 0;
        var sumItogoValue = 0;
        var sumValue = 0;
        var vars = getVarsByCheckbox();
        var itemName = '';
        var oldItem = $rootScope.sections[$rootScope.curentSection]['items'][$rootScope.editItemTrue];
        var defaultItemName = '';
        var isItemNameChanged = false;

        // Если сверлим
        if ($scope.input['work']['id'] === 1) {
            defaultItemName = $scope.input['work'].name + ' Ø' + $scope.input['diameter'].name + '. '
                + ($scope.input['plos'] ? ($scope.input['plos'] + '. ') : '')
                + ($scope.multilayer.isMultilayer ? ($scope.multilayer.multilayerText + '. ') : '')
                + ($scope.input['comment'] || '');
            isItemNameChanged = !oldItem
                || oldItem['work'] !== $scope.input['work']['id']
                || oldItem['diameterName'] !== $scope.input['diameter'].name
                || oldItem['plos'] !== $scope.input['plos']
                || oldItem.multilayer.isMultilayer !== $scope.multilayer.isMultilayer
                || oldItem.multilayer.multilayerText !== $scope.multilayer.multilayerText
                || oldItem['comment'] !== $scope.input['comment'];
            itemName = isItemNameChanged || !$scope.input['itemName']
                ? defaultItemName
                : $scope.input['itemName'];

            //TODO: вызывать getSumByMaterialPricesAndSelectedMaterials до setDataForSelectedMaterials,
            // т.к. нужно сначала определить поля sumItem для selectedMaterials
            sumItemValue = getSumByMaterialPricesAndSelectedMaterials();
            setDataForSelectedMaterials();

            push = {
                'itemName': itemName,
                'work': $scope.input['work']['id'],
                'plos': $scope.input['plos'],
                'comment': $scope.input['comment'],
                'multilayer': $scope.multilayer,
                'selectedMaterials': $scope.selectedMaterials,
                'diameterName': $scope.input['diameter'].name,
                'glubina': $scope.input['glubina'],
                'kolvo': $scope.input['kolvo'],
                'minSum': $scope.input['minSum'],
                'vars': vars,
                'sumItem': common.roundToPrecision(sumItemValue),
                'sumItogo': common.roundToPrecision(getSumItogoForSelectedMaterials()),
                'sum': common.roundToPrecision(getSumForSelectedMaterials())
            };
        }

        // Если режем
        if ($scope.input['work']['id'] === 2) {
            var aparatRezki = $scope.input['work_2'];

            $scope.input['kolvo'] = $scope.input['kolvo'] / 100000;
            defaultItemName = $scope.input['work'].name + '. ' + aparatRezki.name + '. '
                + ($scope.input['plos'] ? ($scope.input['plos'] + '. ') : '')
                + ($scope.multilayer.isMultilayer ? ($scope.multilayer.multilayerText + '. ') : '')
                + ($scope.input['comment'] || '');
            isItemNameChanged = !oldItem
                || oldItem['work'] !== $scope.input['work']['id']
                || oldItem['work_2'] !== aparatRezki.id
                || oldItem['plos'] !== $scope.input['plos']
                || oldItem.multilayer.isMultilayer !== $scope.multilayer.isMultilayer
                || oldItem.multilayer.multilayerText !== $scope.multilayer.multilayerText
                || oldItem['comment'] !== $scope.input['comment'];
            itemName = isItemNameChanged || !$scope.input['itemName']
                ? defaultItemName
                : $scope.input['itemName'];

            //TODO: вызывать getSumByMaterialPricesAndSelectedMaterials до setDataForSelectedMaterials,
            // т.к. нужно сначала определить поля sumItem для selectedMaterials
            sumItemValue = getSumByMaterialPricesAndSelectedMaterials();
            setDataForSelectedMaterials();

            push = {
                'itemName': itemName,
                'work': $scope.input['work']['id'],
                'work_2': aparatRezki.id,
                'plos': $scope.input['plos'],
                'comment': $scope.input['comment'],
                'multilayer': $scope.multilayer,
                'selectedMaterials': $scope.selectedMaterials,
                'kolvo': $scope.input['kolvo'],
                'glubina': $scope.input['glubina'],
                'minSum': $scope.input['minSum'],
                'vars': vars,
                'sumItem': common.roundToPrecision(sumItemValue),
                'sumItogo': common.roundToPrecision(getSumItogoForSelectedMaterials()),
                'sum': common.roundToPrecision(getSumForSelectedMaterials())
            };
        }

        // Если дополнительная услуга
        if ($scope.input['work']['id'] === 3) {
            sumItemValue = $scope.input['edizmPrice_3'];
            sumItogoValue = $scope.sumItogo(sumItemValue);
            sumValue = $scope.input['kolvo'] * $scope.input['edizm_3'] * sumItogoValue;

            push = {
                'work': $scope.input['work']['id'],
                'comment': $scope.input['name_3'],
                'material': $scope.input['material_3'],
                'glubina': $scope.input['glubina_3'],
                'kolvo': $scope.input['kolvo'],
                'edizm_3': $scope.input['edizm_3'],
                'edizmStr_3': $scope.input['edizmStr_3'],
                'sumItem': common.roundToPrecision(sumItemValue),
                'sumItogo': common.roundToPrecision(sumItogoValue),
                'sum': common.roundToPrecision(sumValue)
            };

            if ($scope.input['addSection'] || $scope.input['addProduct']) {
                sumItemValue = $scope.input['addProduct'].sum;
                sumValue = $scope.input['kolvo'] * sumItemValue;

                push = {
                    'work': $scope.input['work']['id'],
                    'comment': $scope.input['addSection'].name + ', ' + $scope.input['addProduct'].name,
                    'kolvo': $scope.input['kolvo'],
                    'edizm_3': 1,
                    'edizmStr_3': $scope.input['addProduct'].edStr,
                    'addSectionName': $scope.input['addSection'].name,
                    'addProductName': $scope.input['addProduct'].name,
                    'sumItem': common.roundToPrecision(sumItemValue),
                    'sumItogo': common.roundToPrecision(sumItemValue),
                    'sum': common.roundToPrecision(sumValue)
                };

            }
        }

        // Если транспортные расходы
        if ($scope.input['work']['id'] === 4) {
            sumItemValue = options.farePrice;
            sumValue = roundToTens(sumItemValue * $scope.input['place'].km * $scope.input['kolvo']);

            push = {
                'work': $scope.input['work']['id'],
                'comment': $scope.input['comment_4'],
                'kolvo': $scope.input['kolvo'],
                'regionName': $scope.input['region'].name,
                'placeName': $scope.input['place'].name,
                'placeDist': $scope.input['place'].km,
                'sumItem': common.roundToPrecision(sumItemValue),
                'sumItogo': common.roundToPrecision(sumItemValue),
                'sum': common.roundToPrecision(sumValue)
            };
        }

        if ($rootScope.editItemTrue === false) {
            $rootScope.sections[$rootScope.curentSection]['items'].push(push);
        } else {
            $rootScope.sections[$rootScope.curentSection]['items'][$rootScope.editItemTrue] = push;
        }

        $rootScope.editItemTrue = false;

        common.itogo();
        $rootScope.isEditSections = common.isEditSections();

        $location.path('/');
    }

    function getSumByMaterialPricesAndSelectedMaterials() {
        return common.MATERIALS_KEYS.reduce(function (result, key) {
            return result + setSumItemForSelectedMaterial($scope.selectedMaterials[key]);
        }, 0);
    }

    function setSumItemForSelectedMaterial(material) {
        if (!material) {
            return 0;
        }
        setCostForPriceMaterial(material);
        material.sumItem = material.priceMaterial && material.priceMaterial.cost
            ? material.priceMaterial.cost
            : 0;

        return material.sumItem;
    }

    function setCostForPriceMaterial(material) {
        if (!material.priceMaterial) {
            material.priceMaterial = {
                cost: 0
            }
        } else {
            var id = material.priceMaterial.id;
            var prices = $scope.input['work']['id'] === 1
                ? $scope.input['diameter'].prices
                : $scope.input['work_2'].prices;

            material.priceMaterial.cost = id && prices[id - 1]
                ? prices[id - 1]
                : 0;
        }
    }

    // Округляет число до десятков рублей
    function roundToTens(value) {
        return Math.ceil(value / 10.0) * 10;
    }

    // Учитываем коэффициенты
    function sumItogo(price) {
        if ($scope.varsplus || $scope.varsMinus) {
            return price * (($scope.varsplus || 0) - ($scope.varsMinus || 0) + 1);
        }

        return price;
    }

    function exit() {
        $location.path('/');
    }

    function isSaveButtonDisabled(form) {
        if ($scope.input['work']['id'] === 1) {
            return !form.kolvo1.$valid
                || !$scope.input['glubina']
                || !isPresentSelctedMaterials();
        }

        if ($scope.input['work']['id'] === 2) {
            return !form.kolvo2.$valid
                || !$scope.input['work_2']
                || !$scope.input['glubina']
                || !isPresentSelctedMaterials();
        }

        return false;
    }

    function isPresentSelctedMaterials() {
        for (var i = 0; i < common.MATERIALS_KEYS.length; i++) {
            if ($scope.selectedMaterials[common.MATERIALS_KEYS[i]]) {
                return true;
            }
        }

        return false;
    }

    function onRegionSelected() {
        $scope.input['place'] = $scope.input['region'].places[0];
    }

    function onAddSectionSelected() {
        $scope.input['addProduct'] = $scope.input['addSection'].products[0];
    }

    function getClassForFactorsColumn(columnIndex) {
        var data = currentPrices.factorsPerColumn;
        var numFactors = options.factors.length;

        return {
            'col-sm-4': data[0] && data[1] && data[2] && (data[0] + data[1]) < numFactors,
            'col-sm-6': data[0] && data[1] && data[0] < numFactors,
            'col-sm-12': (columnIndex === 0 && data[0] >= numFactors) || (!data[1] && !data[2])
        };
    }

    function isColumnVisible(columnIndex) {
        var data = currentPrices.factorsPerColumn;
        var numFactors = options.factors.length;

        if (!data[columnIndex] || !numFactors) {
            return false;
        }

        switch (columnIndex) {
            case 0:
                return true;
            case 1:
                return data[0] < numFactors;
            case 2:
                return data[0] + data[1] < numFactors && data[1];
        }
    }

    // Расчет понижающих коэффициентов
    function varsMinus() {
        var out = 0;

        angular.forEach($scope.checkbox, function (value, key) {
            var factor = currentPrices.getFactorByKey(key);

            if (value && factor.val < 0) {
                out += factor.val;
            }
        });

        $scope.varsMinus = -out;

        if (out === 0) {
            return '';
        }

        return -out.toFixed(2);
    }

    // Расчет повышающих коэффициентов
    function varsPlus() {
        var out = 0;
        var glubina = $scope.input['glubina'] || 1;
        var glubinaFactor = options.factorsAdditionalInfo.glubina;

        angular.forEach($scope.checkbox, function (value, key) {
            var factor = currentPrices.getFactorByKey(key);

            if (value && factor.val > 0) {
                // фактор зависящий от глубины.
                if (factor.code === glubinaFactor.key) {
                    out += glubina >= glubinaFactor.start
                        ? factor.val + factor.val * Math.floor((glubina - glubinaFactor.start) / glubinaFactor.step)
                        : 0;
                } else {
                    out += factor.val;
                }
            }
        });

        $scope.varsplus = out;

        if (out === 0) {
            return '';
        }

        return +out.toFixed(2);
    }

    // Вывод понижающих коэффициентов
    function varsMinusStr() {
        var out = [];

        angular.forEach($scope.checkbox, function (value, key) {
            var factor = currentPrices.getFactorByKey(key);

            if (value === true && factor.val < 0) {
                out.push('K' + factor.name);
            }
        });

        return out
            .sort(varsComparator)
            .join(', ');
    }

    // Вывод повышающих коэффициентов
    function varsPlusStr() {
        var out = [];

        angular.forEach($scope.checkbox, function (value, key) {
            var factor = currentPrices.getFactorByKey(key);

            if (value === true && factor.val >= 0) {
                out.push('K' + factor.name);
            }
        });

        return out
            .sort(varsComparator)
            .join(', ');
    }

    function varsComparator(valL, valR) {
        var numL = parseFloat(valL.slice(1).replace('/', '.'));
        var numR = parseFloat(valR.slice(1).replace('/', '.'));

        return numL - numR;
    }

    function numForFirstColumn() {
        var result = currentPrices.factorsPerColumn[1]
            ? currentPrices.factorsPerColumn[0]
            : options.factors.length;

        return rangeValue(result, 1, options.factors.length);
    }

    function numForSecondColumn() {
        var result = currentPrices.factorsPerColumn[2]
            ? currentPrices.factorsPerColumn[1]
            : options.factors.length - currentPrices.factorsPerColumn[0];

        return rangeValue(result, 0, options.factors.length - numForFirstColumn());
    }

    function numForThirdColumn() {
        var result = options.factors.length - numForFirstColumn() - numForSecondColumn();

        return rangeValue(result, 0, result);
    }

    function rangeValue(value, min, max) {
        if (value < min) {
            return min;
        }

        if (value > max) {
            return max;
        }

        return value;
    }

    function onMaterialSelected(material) {
        $scope.selectedMaterials[$scope.selectingMaterialId] = material;
        $scope.selectedMaterials.description = getDescription(getMaterialDescription, false, true);
        $scope.selectedMaterials.usedPriceMaterials = getDescription(getUsedPriceMaterial, true, true);

        if (material) {
            toggleCheckboxTab($scope.selectingMaterialId);
        }

        $scope.selectingMaterialId = null;
        calcResultGlubina();
        verifyAparatRezkiList();
    }

    function startSelect(materialId) {
        $scope.selectingMaterialId = materialId;
    }

    function calcResultGlubina() {
        var materials = $scope.selectedMaterials;

        $scope.factureChunks = getFactureChunks();
        $scope.input['glubina'] = common.MATERIALS_KEYS.reduce(function (depth, key) {
            if (materials[key] && materials[key].resultDepth) {
                return depth + materials[key].resultDepth;
            }

            return depth;
        }, 0);
    }

    function getDescription(getter, removeDublicates, byLines) {
        var allDescriptions = common.MATERIALS_KEYS.reduce(function (array, key) {
            var value = getter($scope.selectedMaterials[key]);

            if (value) {
                array.push(value);
            }

            return array;
        }, []);

        if (removeDublicates) {
            var finded = {};

            allDescriptions = allDescriptions.filter(function (value) {
                if (finded[value]) {
                    return false;
                }

                finded[value] = true;

                return true;
            });
        }
        return byLines
            ? allDescriptions.join('</br>') + '</br>'
            : allDescriptions.join(' / ');
    }

    function getUsedPriceMaterial(material) {
        if (!material) {
            return '';
        }

        var description = material
            && material.priceMaterial
            && material.priceMaterial.name;

        material.priceMaterialDescription = description
            ? description.charAt(0).toUpperCase() + description.slice(1)
            : '';

        return material.priceMaterialDescription;
    }

    function getMaterialDescription(material) {
        if (!material) {
            return '';
        }

        var commonMaterialInfo = material.commonMaterialInfo;
        var groupNameArray = material.materialGroupName
            ? [material.materialGroupName.charAt(0).toUpperCase() + material.materialGroupName.slice(1)]
            : [];
        var description = material.descriptionKeys
            .reduce(function (result, key) {
                var value = commonMaterialInfo && commonMaterialInfo[key] && commonMaterialInfo[key].label;

                if (value) {
                    result.push(value);
                }

                return result;
            }, groupNameArray)
            .join(', ');

        material.generatedDescription = description || '';

        return description
            ? description + '.'
            : '';
    }

    function clearSelectedMaterials() {
        $scope.selectedMaterials = {
            sideA2: null,
            sideA1: null,
            sideA: null,
            construct: null,
            sideB: null,
            sideB1: null,
            sideB2: null
        };
        $scope.factureChunks = [];
        $scope.currentCheckboxTab = null;
        $scope.checkbox = angular.copy(commonCheckboxData);
        verifyAparatRezkiList();
    }

    function onClearSelectedMaterial() {
        if ($scope.currentCheckboxTab === $scope.selectingMaterialId) {
            $scope.currentCheckboxTab = null;
            $scope.checkbox = angular.copy(commonCheckboxData);
        }
        $scope.selectedMaterials[$scope.selectingMaterialId] = null;
        $scope.selectingMaterialId = null;
        $scope.factureChunks = getFactureChunks();
        verifyAparatRezkiList();
    }

    function toggleCheckboxTab(newTab) {
        var selectedMaterials = $scope.selectedMaterials;

        changeCheckBoxArrayToObject();

        if (!$scope.currentCheckboxTab) {
            if (!angular.equals($scope.checkbox, commonCheckboxData) || $scope.input.discount) {
                commonCheckboxData = angular.copy($scope.checkbox);
                setCommonCheckboxDataForAll();
            }
        } else {
            selectedMaterials[$scope.currentCheckboxTab].checkbox = angular.copy($scope.checkbox);
            selectedMaterials[$scope.currentCheckboxTab].discount = +$scope.input.discount || 0;
        }

        $scope.checkbox = newTab && selectedMaterials[newTab].checkbox
            ? selectedMaterials[newTab].checkbox
            : angular.copy(commonCheckboxData);
        changeCheckBoxArrayToObject();
        $scope.currentCheckboxTab = newTab;

        if (newTab) {
            var value = selectedMaterials[$scope.currentCheckboxTab].discount;

            $scope.input.discount = value || +value === 0
                ? value
                : ($scope.input.discount || 0);
        } else {
            $scope.input.discount = null;
        }
    }

    function setCommonCheckboxDataForAll() {
        var selectedMaterials = $scope.selectedMaterials;

        common.MATERIALS_KEYS.forEach(function (key) {
            var selectedMaterial = selectedMaterials[key];

            if (selectedMaterial) {
                selectedMaterial.checkbox = angular.copy(commonCheckboxData);
                selectedMaterial.discount = +$scope.input.discount
                    ? +$scope.input.discount
                    : selectedMaterial.discount;
            }
        });
    }

    function changeCheckBoxArrayToObject() {
        $scope.checkbox = $scope.checkbox && !Array.isArray($scope.checkbox)
            ? $scope.checkbox
            : {};
    }

    function getVarsByCheckbox(withoutCheckbox) {
        var result = {
            plusSum: varsPlus(),
            minusSum: varsMinus(),
            plusStr: varsPlusStr(),
            minusStr: varsMinusStr()
        };

        if (!withoutCheckbox) {
            result.checkbox = $scope.checkbox;
        }

        return result;
    }

    function getSumItogoForSelectedMaterials() {
        return common.MATERIALS_KEYS.reduce(function (res, key) {
            return res + (($scope.selectedMaterials[key] && $scope.selectedMaterials[key].sumItogo) || 0);
        }, 0);
    }

    function getSumForSelectedMaterials() {
        return common.MATERIALS_KEYS.reduce(function (res, key) {
            return res + (($scope.selectedMaterials[key] && $scope.selectedMaterials[key].sum) || 0);
        }, 0);
    }

    function setDataForSelectedMaterials() {
        var selectedMaterials = $scope.selectedMaterials;
        var bckp = $scope.checkbox;

        common.MATERIALS_KEYS.forEach(function (key) {
            var selectedMaterial = selectedMaterials[key];

            if (selectedMaterial) {
                if (!selectedMaterial.checkbox) {
                    selectedMaterial.checkbox = commonCheckboxData;
                }

                $scope.checkbox = selectedMaterial.checkbox;

                selectedMaterial.vars = getVarsByCheckbox(true);
                selectedMaterial.discount = selectedMaterial.discount || +$scope.input.discount || 0;
                $scope.varsMinus = $scope.varsMinus + selectedMaterial.discount;

                var sumItogoValue = $scope.sumItogo(selectedMaterial.sumItem);
                var sumValue = sumItogoValue * $scope.input['kolvo'] * selectedMaterial.resultDepth;

                selectedMaterial.sumItogo = common.roundToPrecision(sumItogoValue);
                selectedMaterial.sum = common.roundToPrecision(sumValue);
            }
        });

        $scope.checkbox = bckp;
    }

    function onDiscountChange() {
        $scope.input.discount = common.parseInputPercentsFromString($scope.input.discount);
    }

    function onMinSumChange() {
        $scope.input.minSum = common.parseInputPercentsFromString($scope.input.minSum);
    }

    function getDiscountValueForInput() {
        return !$scope.currentCheckboxTab
            ? null
            : $scope.selectedMaterials[$scope.currentCheckboxTab].discount || 0;
    }

    function getFirstSelectedMaterialKey() {
        for (var i = 0; i < common.MATERIALS_KEYS.length; i++) {
            if ($scope.selectedMaterials[common.MATERIALS_KEYS[i]]) {
                return common.MATERIALS_KEYS[i];
            }
        }
    }

    function onWorkTypeChange() {
        if ($scope.input['work']['id'] === 1) {
            $scope.materialsForMaterialSelector = options.materials;
            $scope.input['kolvo'] = 1;
        }

        if ($scope.input['work']['id'] === 2) {
            $scope.materialsForMaterialSelector = options.materialRezki;
            $scope.input['kolvo'] = 1000;
        }

        currentPrices.setFactorsByWorkId($scope.input['work']['id']);
        clearAllCheckBoxes();
    }

    function clearAllCheckBoxes() {
        $scope.checkbox = {};

        for (var i = 0; i < common.MATERIALS_KEYS.length; i++) {
            if ($scope.selectedMaterials[common.MATERIALS_KEYS[i]]) {
                $scope.selectedMaterials[common.MATERIALS_KEYS[i]].checkbox = {};
            }
        }
    }

    function isSelectedMaterialButtonVisible(key) {
        if (common.ALWAYS_SHOWED_BUTTONS_FOR_MATERIAL_SELECTING[key]) {
            return true;
        }

        return $scope.selectedMaterials[key] || $scope.selectedMaterials[common.getPrevLayerKey(key)];
    }

    function verifyAparatRezkiList() {
        if ($scope.input['work']['id'] !== 2) {
            return;
        }

        var usedPriceMaterialsIds = getUsedPriceMaterialsIds();

        setAvailableAparatsRezki(usedPriceMaterialsIds);

        if ($scope.input.work_2 && !isValidAparatRezki($scope.input.work_2, usedPriceMaterialsIds)) {
            $scope.input.work_2 = null;
        }
    }

    function getUsedPriceMaterialsIds() {
        var result = [];

        for (var i = 0; i < common.MATERIALS_KEYS.length; i++) {
            var material = $scope.selectedMaterials[common.MATERIALS_KEYS[i]];

            if (material && material.priceMaterial) {
                result.push(material.priceMaterial.id);
            }
        }

        return result;
    }

    function setAvailableAparatsRezki(usedPriceMaterialsIds) {
        $scope.availableAparatRezki = options.aparatRezki.filter(function (aparatRezki) {
            return isValidAparatRezki(aparatRezki, usedPriceMaterialsIds);
        });
    }

    function isValidAparatRezki(aparatRezki, usedPriceMaterialsIds) {
        for (var i = 0; i < usedPriceMaterialsIds.length; i++) {
            if (!aparatRezki.prices[usedPriceMaterialsIds[i] - 1]) {
                return false;
            }
        }

        return true;
    }

    function getFactureChunks() {
        var result = [];

        for (var i = 0; i < common.MATERIALS_KEYS.length; i++) {
            var material = $scope.selectedMaterials[common.MATERIALS_KEYS[i]];

            if (material) {
                result.push({
                    facture: material.facture,
                    weight: material.resultDepth
                });
            }
        }

        return result;
    }

    function moveLayerLeft(index, event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        var key1 = common.MATERIALS_KEYS[index];
        var key2 = common.MATERIALS_KEYS[index - 1];

        swapByKeys($scope.selectedMaterials, key1, key2);
        updateAfterLayersChanged(key1, key2);
    }

    function moveLayerRight(index, event) {
        event.preventDefault();
        event.stopImmediatePropagation();

        var key1 = common.MATERIALS_KEYS[index];
        var key2 = common.MATERIALS_KEYS[index + 1];

        swapByKeys($scope.selectedMaterials, key1, key2);
        updateAfterLayersChanged(key1, key2);
    }

    function updateAfterLayersChanged(key1, key2) {
        $scope.selectedMaterials.description = getDescription(getMaterialDescription, false, true);

        if ($scope.currentCheckboxTab === key1) {
            $scope.currentCheckboxTab = key2;
        } else if ($scope.currentCheckboxTab === key2) {
            $scope.currentCheckboxTab = key1;
        }

        calcResultGlubina();
    }

    function swapByKeys(obj, key1, key2) {
        var temp = obj[key1];

        obj[key1] = obj[key2];
        obj[key2] = temp;
    }
}
