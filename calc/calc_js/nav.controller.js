angular.module('calc')
    .controller('navCtrl', navController);

navController.$inject = ['$rootScope', '$scope', '$http', '$location', 'currentPricesService', 'commonService',
    'usedFactorsService'];

function navController($rootScope, $scope, $http, $location, currentPrices, common, usedFactorsService) {
    /*
    № КоличествоСтолбцов НазваниеСтолбца
    0 (1) - № п/п
    1 (1) - Наименование работы, услуги
    2 (1) - Материал
    3 (1) - Глубина, см
    4 (2) - Количество (шт. \ ед.изм.)
    5 (1) - Ед. изм.
    6 (1) - Цена за ед. изм., руб.
    7 (5) - Расчет стоимости (коэффициент (понижающий (к \ знач.) \ повышающий (к \ знач.)) \ цена с коэф., руб.)
    8 (1) - Сумма, руб.
     */
    var tableFormats = [
        {hiddenCollumns: {length: 0}, label: 'Полный'},
        {hiddenCollumns: {6: true, 7: true, 8: true, length: 7}, label: 'Для тех что-то там'}
    ];
    var formatsBackup;

    $rootScope.hiddenCollumns = tableFormats[0].hiddenCollumns;
    $rootScope.isDetailedView = true;
    $rootScope.isSelectedFormatWasChanged = false;
    $scope.tableFormats = tableFormats;
    $scope.selectedTableFormat = tableFormats[0];
    $scope.isFormatsEditing = false;
    $scope.isUserCanEdit = window.isUserCanEdit;
    $scope.save = save;
    $scope.search = search;
    $scope.print = print;
    $scope.clear = clear;
    $scope.downloadExcel = downloadExcel;
    $scope.onTableFormatSelected = onTableFormatSelected;
    $scope.toggleFormatsEditMode = toggleFormatsEditMode;
    $scope.addFormat = addFormat;
    $scope.removeFormat = removeFormat;
    $scope.saveFormats = saveFormats;
    $scope.reloadFormats = reloadFormats;
    $scope.toggleDetailedView = toggleDetailedView;

    init();

    function init() {
        // TODO Local
        $http.get('/mock/formats.json')
        // TODO Release
        // $http.get('https://kubdem.ru/calcdev/formats')
            .then(function (responce) {
                if (responce.data && responce.data.length) {
                    $scope.tableFormats = responce.data.map(function (t) {
                        return t.item || t
                    });
                    onTableFormatSelected($scope.tableFormats[0]);
                    $rootScope.isSelectedFormatWasChanged = false;
                }
            });
    }

    function reloadFormats() {
        init();
    }

    function saveFormats() {
        // TODO Local
        $http.post('/saveformats', {items: $scope.tableFormats})
        // TODO Release
        // $http.post('https://kubdem.ru/calcdev/saveformats', {items: $scope.tableFormats})
            .then(function (responce) {
                $rootScope.isSelectedFormatWasChanged = false;
            });
    }

    function save() {
        var data = {
            calc: {
                sections: $rootScope.sections,
                clientDiscount: $rootScope.sections.clientDiscount,
                nds: $rootScope.nds,
                priceId: currentPrices.priceId,
                minPrice: currentPrices.minPrice,
                isMinPriceEdited: currentPrices.isMinPriceEdited,
                options: !currentPrices.priceId && currentPrices.options
            }
        };

        // TODO Local
        $http.post('/save', data)
        // TODO Release
        // $http.post('https://kubdem.ru/calc/save', data)
            .then(function (responce) {
                var data = responce.data;

                if (data === false) {
                    alert('Ошибка сохранения! Попробуйте еще раз.');
                } else {
                    $rootScope.calcId = data;
                    alert('Расчет сохранен под № ' + data);
                    $location.path('/item/' + data + '/print/true');
                }
            });
    }

    function search() {
        $location.path('/item/' + $rootScope.calcId + '/print/false');
    }

    function print() {
        $location.path('/item/' + $rootScope.calcId + '/print/true');
    }

    function clear() {
        common.clearRootScope();
        currentPrices.clear();
        usedFactorsService.clear();
    }

    function downloadExcel() {
        common.downloadExcel('.my-table', 'расчет_' + $rootScope.calcId + '.xls');
    }

    function onTableFormatSelected(tableFormat) {
        $scope.selectedTableFormat = tableFormat;
        $rootScope.hiddenCollumns = $scope.selectedTableFormat.hiddenCollumns;
    }

    function toggleFormatsEditMode(isNeedRestoreBackup) {
        if (!$scope.isFormatsEditing) {
            formatsBackup = angular.copy($scope.tableFormats);
        } else if (!isNeedRestoreBackup) {
            saveFormats();
        }

        $scope.isFormatsEditing = !$scope.isFormatsEditing;

        if (isNeedRestoreBackup) {
            $scope.tableFormats = formatsBackup;
            onTableFormatSelected($scope.tableFormats[0]);
        }
    }

    function addFormat() {
        $scope.tableFormats.push({
            label: 'Полный',
            hiddenCollumns: {
                length: 0
            }
        });
    }

    function removeFormat(formatIndex) {
        $scope.tableFormats.splice(formatIndex, 1);
        onTableFormatSelected($scope.tableFormats[0]);
    }

    function toggleDetailedView() {
        $rootScope.isDetailedView = !$rootScope.isDetailedView;
    }
}
