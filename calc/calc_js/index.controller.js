angular.module('calc')
    .controller('indexCtrl', indexController);

indexController.$inject = ['$rootScope', '$scope', '$location', 'focus', 'currentPricesService',
    'usedFactorsService', 'commonService'];

function indexController($rootScope, $scope, $location, focus, currentPrices, usedFactorsService, common) {
    var NUM_COLLUMNS_PER_COLLUMN = {0: 1, 1: 1, 2: 1, 3: 1, 4: 2, 5: 1, 6: 1, 7: 5, 8: 1};
    var options = currentPrices.options;

    // Сервисы
    $scope.common = common;
    $scope.currentPrices = currentPrices;
    $scope.usedFactorsService = usedFactorsService;

    // Методы
    $scope.isNumeric = isNumeric;
    $scope.ifNds = ifNds;
    $scope.nds = nds;
    $scope.editPriceOk = editPriceOk;
    $scope.editPrice = editPrice;
    $scope.editItem = editItem;
    $scope.cloneItem = cloneItem;
    $scope.cloneSection = cloneSection;
    $scope.editSection = editSection;
    $scope.addSection = addSection;
    $scope.delSection = delSection;
    $scope.addItem = addItem;
    $scope.delItem = delItem;
    $scope.editMinPrice = editMinPrice;
    $scope.changeMinPrice = changeMinPrice;
    $scope.toggleCollumnVisibility = toggleCollumnVisibility;
    $scope.onDiscountChange = onDiscountChange;
    $scope.onClientDiscountChange = onClientDiscountChange;
    $scope.delSelectedMaterial = delSelectedMaterial;
    $scope.toggleClientDiscountCheckbox = toggleClientDiscountCheckbox;

    $scope.globalItemIndex = 0;
    $scope.isUserCanEdit = window.isUserCanEdit;
    $scope.options = options;
    $scope.price = [];
    $scope.isEditMinPrice = false;
    $scope.customMinPrice = currentPrices.minPrice;
    $scope.input = {
        clientDiscount: $rootScope.sections && $rootScope.sections.clientDiscount
            ? $rootScope.sections.clientDiscount.toString()
            : '0'
    };

    $rootScope.isSearchBarVisible = true;
    $rootScope.curentSection = false;
    $rootScope.editItemTrue = false;

    if (!$rootScope.nds) {
        $rootScope.nds = false;
    }

    if (!$rootScope.sections) {
        $rootScope.sections = [];
        $rootScope.sections.discount = 0;
        $rootScope.sections.clientDiscount = 0;
        $rootScope.sections.isClientDiscount = false;
        $rootScope.sections.itogo = 0;
        $rootScope.isEditSections = false;
        $rootScope.isPrint = false;
    }

    usedFactorsService.updateUsedFactors();

    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    function ifNds() {
        return $rootScope.nds;
    }

    function nds() {
        $rootScope.nds = !$rootScope.nds;
        $rootScope.isEditSections = true;
        $rootScope.isPrint = true;
    }

    function editPriceOk(key, k, priceOne) {
        $rootScope.isEditSections = true;
        $rootScope.isPrint = true;

        var el = $rootScope.sections[key]['items'][k];
        var indx1 = 1;
        var indx2 = 1;

        // проверяем на число
        if ($scope.isNumeric(priceOne) && priceOne * 1 != 0) {
            indx1 = el['sumItogo'] / el['sumItem'];
            indx2 = el['sum'] / el['sumItem'];
            el['sumItem'] = priceOne * 1;
            el['sumItogo'] = priceOne * indx1;
            el['sum'] = priceOne * indx2;
            el['priceStyle'] = {color: 'red'};
            common.itogo();
        }
    }

    function editPrice(key, k) {
        if ($rootScope.sections[key]['items'][k]['sumItem'] === 0) {
            return;
        }
        var kk = (key + 1) + '-' + (k + 1);
        $scope.price[kk] = true;
        focus(kk);
    }

    function editItem(key, id) {
        $rootScope.curentSection = key;
        $rootScope.editItemTrue = id;
        $location.path('/addItem');
    }

    function cloneSection(key) {
        var sectionNumber = key + 1;
        var push = angular.copy($rootScope.sections[key]);
        var comment = '(копия раздела ' + sectionNumber + ')';

        if (!confirm('Клонировать раздел №' + sectionNumber + '?')) {
            return;
        }

        $rootScope.sections.push(push);
        $rootScope.isEditSections = common.isEditSections();
        common.itogo();
    }

    function cloneItem(key, k) {
        var sectionNumber = key + 1;
        var itemNumber = k + 1;
        var item = $rootScope.sections[key]['items'][k];
        var push = angular.copy(item);

        if (!confirm('Клонировать услугу № ' + sectionNumber + '.' + itemNumber + ' ?')) {
            return;
        }

        push.comment = push.comment || '';

        if (push.work === 1 || push.work === 2) {
            push.comment = push.comment + ' К позиции ' + sectionNumber + '.' + itemNumber + '. ';
        }

        $rootScope.sections[key]['items'].push(push);
        $rootScope.isEditSections = common.isEditSections();
        common.itogo();
    }

    function editSection(key) {
        $rootScope.curentSection = key;
        $location.path('/addSection');
    }

    function addSection() {
        $location.path('/addSection');
    }

    function delSection(key) {
        if (!confirm('Удалить раздел?')) {
            return;
        }
        $rootScope.sections.splice(key, 1);

        if ($rootScope.sections.length === 0) {
            $rootScope.sections.itogo = 0;
            $rootScope.sections.discount = 0;
            $rootScope.sections.clientDiscount = 0;
            $rootScope.sections.isClientDiscount = false;
        }
        $rootScope.isEditSections = common.isEditSections();
        common.itogo();
        usedFactorsService.updateUsedFactors();
    }

    function addItem(key) {
        $rootScope.curentSection = key;
        $location.path('/addItem');
    }

    function delItem(key, k) {
        if (!confirm('Удалить услугу?')) {
            return;
        }
        $rootScope.sections[key]['items'].splice(k, 1);

        common.itogo();
        usedFactorsService.updateUsedFactors();
        $rootScope.isEditSections = common.isEditSections();
    }

    function editMinPrice() {
        $scope.isEditMinPrice = !$scope.isEditMinPrice;
    }

    function changeMinPrice() {
        if ($scope.customMinPrice > 0) {
            currentPrices.isMinPriceEdited = currentPrices.isMinPriceEdited || currentPrices.minPrice !== $scope.customMinPrice;
            currentPrices.minPrice = $scope.customMinPrice;
        } else {
            $scope.customMinPrice = currentPrices.minPrice;
        }

        $scope.isEditMinPrice = false;
        common.itogo();
    }

    function toggleCollumnVisibility(collumnIndex) {
        var numCollumns = $rootScope.hiddenCollumns[collumnIndex]
            ? -NUM_COLLUMNS_PER_COLLUMN[collumnIndex]
            : NUM_COLLUMNS_PER_COLLUMN[collumnIndex];

        $rootScope.hiddenCollumns[collumnIndex] = !$rootScope.hiddenCollumns[collumnIndex];
        $rootScope.hiddenCollumns.length += numCollumns;
        $rootScope.isSelectedFormatWasChanged = true;
    }

    function onDiscountChange(item, selectedMaterial) {
        selectedMaterial.discount = common.parseInputPercentsFromString(selectedMaterial.discount);

        var discount = +selectedMaterial.discount || 0;
        var sumItogoValue = selectedMaterial.sumItem
            * ((selectedMaterial.vars.plusSum || 0) - (selectedMaterial.vars.minusSum || 0) - discount + 1);
        var sumValue = sumItogoValue * item.kolvo * selectedMaterial.resultDepth;

        selectedMaterial.sumItogo = common.roundToPrecision(sumItogoValue);
        selectedMaterial.sum = common.roundToPrecision(sumValue);

        recalcSumsForItem(item);

        common.itogo();
    }

    function onClientDiscountChange() {
        $scope.input.clientDiscount = common.parseInputPercentsFromString($scope.input.clientDiscount);
        $rootScope.sections.clientDiscount = +$scope.input.clientDiscount;
        $rootScope.isEditSections = $rootScope.sections.isClientDiscount === $rootScope.cloneSections.isClientDiscount
            ? $rootScope.sections.clientDiscount !== $rootScope.cloneSections.clientDiscount
            : true;
        $rootScope.isPrint = $rootScope.isEditSections;
        common.itogo();
    }

    function recalcSumsForItem(item) {
        var sumItogoValue = common.MATERIALS_KEYS.reduce(function (result, key) {
            return result + (item.selectedMaterials[key] ? item.selectedMaterials[key].sumItogo : 0);
        }, 0);
        var sumValue = common.MATERIALS_KEYS.reduce(function (result, key) {
            return result + (item.selectedMaterials[key] ? item.selectedMaterials[key].sum : 0);
        }, 0);

        item.sumItogo = common.roundToPrecision(sumItogoValue);
        item.sum = common.roundToPrecision(sumValue);
    }

    function delSelectedMaterial(item, materialKey) {
        if (confirm('Удалить слой?')) {
            item.selectedMaterials[materialKey] = null;
            recalcSumsForItem(item);
            common.itogo();
            usedFactorsService.updateUsedFactors();
            $rootScope.isEditSections = common.isEditSections();
        }
    }

    function toggleClientDiscountCheckbox() {
        $rootScope.sections.isClientDiscount = !$rootScope.sections.isClientDiscount;

        if ($rootScope.sections.isClientDiscount) {
            onClientDiscountChange();
        } else {
            $rootScope.isEditSections = $rootScope.sections.isClientDiscount !== $rootScope.cloneSections.isClientDiscount;
            $rootScope.isPrint = $rootScope.isEditSections;
            common.itogo();
        }

    }
}
