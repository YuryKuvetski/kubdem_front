angular.module('calc')
    .directive('svgDrawer', svgDrawer);

svgDrawer.$inject = ['$location', 'commonService'];

function svgDrawer($location, commonService) {
    return {
        restrict: 'E',
        scope: {
            chunks: '=',
            isCircle: '=',
            height: '<'
        },
        templateUrl: 'calc_js/materialSelector/svgDrawer/svgDrawer.template.html',
        link: function ($scope) {
            $scope.commonService = commonService;
            $scope.validChunks = getValidNormalizedChunks();
            $scope.absUrl = $location.absUrl();
            $scope.height = $scope.height || 80;
            $scope.FACTURES_LABELS = commonService.FACTURES_LABELS;

            $scope.$watch('chunks', function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    $scope.validChunks = getValidNormalizedChunks();
                }
            });

            function getValidNormalizedChunks() {
                var validChunks = $scope.chunks.filter(function (chunk) {
                    return chunk.weight;
                });
                var sumWeight = getSumWeight(validChunks);
                var fromStart = 0;

                return validChunks.map(function (chunk) {
                    var result = {
                        facture: chunk.facture,
                        weight: (chunk.weight * 100) / sumWeight,
                        fromStart: fromStart
                    };

                    fromStart += result.weight;

                    return result;
                });
            }

            function getSumWeight(chunks) {
                return chunks.reduce(function (sum, chunk) {
                    return sum + (chunk.weight || 0);
                }, 0);
            }
        }
    };
}
