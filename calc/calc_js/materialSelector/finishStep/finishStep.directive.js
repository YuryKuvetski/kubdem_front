angular.module('calc')
    .directive('finishStep', finishStep);

finishStep.$inject = ['commonService'];

function finishStep(commonService) {
    return {
        restrict: 'E',
        scope: {
            materials: '=',
            masonryTypes: '=',
            proportionsLabels: '=',
            selectedMaterialGroupItem: '=',
            resultMaterial: '=',
            isStepFulfilled: '=',
            fieldNames: '=',
            isEditMode: '=',
            onMasonryTypesChanged: '&',
            onPurposeChanged: '&'

        },
        templateUrl: 'calc_js/materialSelector/finishStep/finishStep.template.html',
        controller: finishStepController,
        controllerAs: '$ctrl'
    };

    function finishStepController($scope) {
        var vm = this;
        var DEFAULT_PROPORTIONS_ORDER_KEY = 'width';
        var manufacturersBackup = null;
        var DEFAULT_NEW_PROPORTIONS = {
            length: 1,
            width: 1,
            height: 1,
            label: '',
            nf: 0,
            nf_extra: 0
        };

        vm.selectMasonryType = selectMasonryType;
        vm.isMasonryTypeSelected = isMasonryTypeSelected;
        vm.selectMasonryTypeVariation = selectMasonryTypeVariation;
        vm.selectProportions = selectProportions;
        vm.isProportionsSelected = isProportionsSelected;
        vm.isFirstPlusSignVisible = isFirstPlusSignVisible;
        vm.isSecondPlusSignVisible = isSecondPlusSignVisible;
        vm.isThirdPlusSignVisible = isThirdPlusSignVisible;
        vm.addProportions = addProportions;
        vm.removeProportions = removeProportions;
        vm.onProportionsChange = onProportionsChange;
        vm.decrementNf = decrementNf;
        vm.incrementNf = incrementNf;
        vm.addMasonryType = addMasonryType;
        vm.addVariation = addVariation;
        vm.removeVariation = removeVariation;
        vm.onPurposeChanged = onPurposeChanged;
        vm.onMasonryTypesChanged = onMasonryTypesChanged;
        vm.toggleMasonryTypeEditMode = toggleMasonryTypeEditMode;
        vm.removeMasonryType = removeMasonryType;
        vm.calcResultDepth = calcResultDepth;
        vm.getClassForProportionsOrderCheckbox = getClassForProportionsOrderCheckbox;
        vm.setProportionsOrder = setProportionsOrder;
        vm.clearProportionsOrderForKey = clearProportionsOrderForKey;
        vm.addManufacturer = addManufacturer;
        vm.toggleManufacturerEditMode = toggleManufacturerEditMode;
        vm.selectManufacturer = selectManufacturer;
        vm.removeManufacturer = removeManufacturer;
        vm.isDefaultManufacturer = isDefaultManufacturer;
        vm.setDefaultManufacturer = setDefaultManufacturer;
        vm.moveLeftMasonryType = moveLeftMasonryType;
        vm.moveRightMasonryType = moveRightMasonryType;
        vm.onCustomProportionsModeChange = onCustomProportionsModeChange;
        vm.getPrintedNf = getPrintedNf;
        vm.moveManufacturerUp = moveManufacturerUp;
        vm.moveManufacturerDown = moveManufacturerDown;
        vm.isVisibleColumn = isVisibleColumn;
        vm.toggleVisibleColumn = toggleVisibleColumn;
        vm.isVisibleMasonryType = isVisibleMasonryType;
        vm.toggleVisibleMasonryType = toggleVisibleMasonryType;
        vm.isUserCanEdit = window.isUserCanEdit;
        vm.masonryTypes = angular.copy($scope.masonryTypes);
        vm.commonMaterialInfo = null;
        vm.resultMaterial = $scope.resultMaterial;
        vm.isUserCanEdit = window.isUserCanEdit;
        vm.editedMasonryTypeIndex = null;
        vm.fieldsOrder = [];
        vm.selectedManufacturer = null;
        vm.selectedManufacturerIndex = null;
        vm.isManufacturerEditing = false;
        vm.isProportionsLabelsEditing = false;
        vm.isCustomProportionsMode = false;

        init();

        function init() {
            removeUnusedFields();
            vm.fieldsOrder = getFieldsOrder();
            vm.commonMaterialInfo = angular.copy($scope.resultMaterial.commonMaterialInfo);

            initPurposeData();

            if (vm.resultMaterial.proportions) {
                initProportions();
            }

            if (vm.resultMaterial.masonryType) {
                initMasonryType();
            } else {
                selectMasonryTypeIfSingle();
            }

            calcResultPriceMaterial();
            calcResultDepth();
        }

        function initPurposeData() {
            var purposeData = vm.commonMaterialInfo.purpose.data;

            if (!purposeData.orderKeysInfo) {
                purposeData.orderKeysInfo = [{
                    key: DEFAULT_PROPORTIONS_ORDER_KEY,
                    isReverse: false,
                    index: 0
                }];
            }

            if (purposeData.orderKey) {
                delete purposeData.orderKey;
            }

            if (purposeData.isOrderReverse || purposeData.isOrderReverse === false) {
                delete purposeData.isOrderReverse;
            }

            vm.selectedManufacturerIndex = purposeData.defaultManufacturerIndex || 0;
            vm.selectedManufacturer = purposeData.manufacturers[vm.selectedManufacturerIndex];
            sortProportions();
        }

        function getFieldsOrder() {
            return $scope.selectedMaterialGroupItem.fields.map(function (field) {
                return field.key;
            })
        }

        function removeUnusedFields() {
            for (var fieldKey in vm.resultMaterial.commonMaterialInfo) {
                if (vm.resultMaterial.commonMaterialInfo.hasOwnProperty(fieldKey)) {
                    if (!vm.resultMaterial.commonMaterialInfo[fieldKey]) {
                        delete vm.resultMaterial.commonMaterialInfo[fieldKey];
                    }
                }
            }
        }

        function calcResultPriceMaterial() {
            for (var fieldKey in vm.resultMaterial.commonMaterialInfo) {
                if (vm.resultMaterial.commonMaterialInfo.hasOwnProperty(fieldKey)) {
                    var data = vm.resultMaterial.commonMaterialInfo[fieldKey].data;

                    vm.resultMaterial.materialId = data && data.materialId || vm.resultMaterial.materialId || 1;
                }
            }

            for (var i = 0; i < $scope.materials.length; i++) {
                if ($scope.materials[i].id === vm.resultMaterial.materialId) {
                    vm.resultMaterial.priceMaterial = $scope.materials[i];
                    delete vm.resultMaterial.materialId;

                    return;
                }
            }
        }

        function initProportions() {
            var resultProportions = vm.resultMaterial.proportions;

            if (resultProportions && (resultProportions.manufacturerIndex || resultProportions.manufacturerIndex === 0)) {
                vm.selectedManufacturerIndex = resultProportions.manufacturerIndex;
                vm.selectedManufacturer = vm.commonMaterialInfo.purpose.data.manufacturers[vm.selectedManufacturerIndex];
            }

            for (var i = 0; i < vm.selectedManufacturer.proportionsArray.length; i++) {
                if (isProportionsSelected(vm.selectedManufacturer.proportionsArray[i])) {
                    return;
                }
            }

            if (vm.resultMaterial.proportions) {
                vm.isCustomProportionsMode = true;
            } else {
                vm.resultMaterial.proportions = null;
            }
        }

        function initMasonryType() {
            for (var i = 0; i < vm.masonryTypes.length; i++) {
                if (isMasonryTypeSelected(vm.masonryTypes[i], true)) {
                    return;
                }
            }

            vm.resultMaterial.masonryType = null;
        }

        function selectMasonryTypeIfSingle() {
            var visibleMasonryType = null;
            var numVisibleTypes = !vm.masonryTypes ? 0 : vm.masonryTypes.reduce(function (count, masonryType) {
                if (!masonryType.hiddenIn || !masonryType.hiddenIn[vm.selectedManufacturer.label]) {
                    visibleMasonryType = masonryType;
                    count++;
                }

                return count;
            }, 0);

            if (numVisibleTypes === 1) {
                selectMasonryType(visibleMasonryType, true);
            }
        }

        function selectProportions(proportions) {
            if ($scope.isEditMode) {
                return;
            }

            vm.resultMaterial.proportions = proportions;
            vm.resultMaterial.proportions.manufacturerIndex = vm.selectedManufacturerIndex;
            calcResultDepth();
        }

        function isProportionsSelected(proportions) {
            return vm.resultMaterial
                && vm.resultMaterial.proportions
                && vm.resultMaterial.proportions.manufacturerIndex === vm.selectedManufacturerIndex
                && vm.resultMaterial.proportions.nf === proportions.nf
                && vm.resultMaterial.proportions.nf_extra === proportions.nf_extra
                && vm.resultMaterial.proportions.label === proportions.label
                && vm.resultMaterial.proportions.width === proportions.width
                && vm.resultMaterial.proportions.height === proportions.height
                && vm.resultMaterial.proportions.length === proportions.length;
        }

        function selectMasonryType(masonryType, checkIfEditMode) {
            if (!checkIfEditMode && $scope.isEditMode) {
                return;
            }

            if (!masonryType.selectedVariationIndex && masonryType.selectedVariationIndex !== 0) {
                vm.resultMaterial.masonryType = {
                    label: masonryType.label,
                    data: masonryType.defaultData
                };
            } else {
                vm.resultMaterial.masonryType = masonryType.variations[masonryType.selectedVariationIndex];
            }
            calcResultDepth();
        }

        function isMasonryTypeSelected(masonryType, needCheckOrSelectOtherVariations, checkIfEditMode) {
            if (!vm.resultMaterial.masonryType || (!checkIfEditMode && $scope.isEditMode)) {
                return false;
            }

            if (!masonryType.selectedVariationIndex && masonryType.selectedVariationIndex !== 0) {
                return vm.resultMaterial.masonryType.label === masonryType.label;
            }

            if (needCheckOrSelectOtherVariations) {
                for (var i = 0; i < masonryType.variations.length; i++) {
                    if (vm.resultMaterial.masonryType.label === masonryType.variations[i].label) {
                        vm.resultMaterial.masonryType.selectedVariationIndex = i;
                        masonryType.selectedVariationIndex = i;
                        return true;
                    }
                }

                return false;
            }

            return vm.resultMaterial.masonryType.label === masonryType.variations[masonryType.selectedVariationIndex].label;
        }

        function selectMasonryTypeVariation(masonryType, variationIndex) {
            masonryType.selectedVariationIndex = variationIndex;

            if ($scope.isEditMode) {
                onMasonryTypesChanged();
            }

            if (isMasonryTypeSelected(masonryType)) {
                selectMasonryType(masonryType);
            } else {
                calcResultDepth();
            }
        }

        function calcResultDepth() {
            $scope.isStepFulfilled = !!vm.resultMaterial.proportions && !!vm.resultMaterial.masonryType;
            if ($scope.isStepFulfilled) {
                vm.resultMaterial.resultDepth =
                    ((vm.resultMaterial.masonryType.data.length || 0) * (vm.resultMaterial.proportions.length || 0)) +
                    ((vm.resultMaterial.masonryType.data.width || 0) * (vm.resultMaterial.proportions.width || 0)) +
                    ((vm.resultMaterial.masonryType.data.height || 0) * (vm.resultMaterial.proportions.height || 0)) +
                    ((vm.resultMaterial.masonryType.data.plaster || 0) * 10);
                vm.resultMaterial.resultDepth = vm.resultMaterial.resultDepth / 10;
                vm.resultMaterial.resultDepthFormatted = vm.resultMaterial.resultDepth.toFixed(2);
            }
        }

        function isFirstPlusSignVisible() {
            return vm.resultMaterial.masonryType.data.length
                && (vm.resultMaterial.masonryType.data.width
                    || vm.resultMaterial.masonryType.data.height
                    || vm.resultMaterial.masonryType.data.plaster);
        }

        function isSecondPlusSignVisible() {
            return vm.resultMaterial.masonryType.data.width
                && (vm.resultMaterial.masonryType.data.height
                    || vm.resultMaterial.masonryType.data.plaster);
        }

        function isThirdPlusSignVisible() {
            return vm.resultMaterial.masonryType.data.height
                && vm.resultMaterial.masonryType.data.plaster;
        }

        function addMasonryType() {
            vm.masonryTypes.push({
                label: 'название',
                variations: [],
                selectedVariationIndex: null,
                defaultData: {
                    length: 0,
                    width: 0,
                    height: 0,
                    plaster: 0
                }
            });
            onMasonryTypesChanged();
        }

        function addVariation(masonryType) {
            if (!masonryType.variations || !masonryType.variations.length) {
                masonryType.variations = [
                    {
                        label: masonryType.label,
                        data: masonryType.defaultData
                    }
                ];
                masonryType.selectedVariationIndex = 0;
            }

            masonryType.variations.push({
                label: 'Название',
                data: {
                    'length': 0,
                    'width': 0,
                    'height': 0,
                    'plaster': 0
                }
            });

            onMasonryTypesChanged();
        }

        function removeVariation(masonryType, variationIndex) {
            var itWasSelected = isMasonryTypeSelected(masonryType, false, true);
            masonryType.variations.splice(variationIndex, 1);

            if (masonryType.variations.length === 1) {
                masonryType.defaultData = masonryType.variations[0].data;
                masonryType.variations = [];
                masonryType.selectedVariationIndex = null;
            } else if (!masonryType.variations[masonryType.selectedVariationIndex]
                || masonryType.selectedVariationIndex >= variationIndex) {
                masonryType.selectedVariationIndex = masonryType.variations.length
                    ? 0
                    : null;
            }

            if (isMasonryTypeSelected(masonryType, false, true)) {
                calcResultDepth();
            } else if (itWasSelected) {
                selectMasonryType(masonryType, true);
            }

            onMasonryTypesChanged();
        }

        function addProportions() {
            vm.selectedManufacturer.proportionsArray.push(angular.copy(DEFAULT_NEW_PROPORTIONS));
            onPurposeChanged();
        }

        function removeProportions(proportions) {
            var array = vm.selectedManufacturer.proportionsArray;
            var isFindedSelected = false;
            var proportionsIndex = array.indexOf(proportions);

            if (proportionsIndex > -1) {
                array.splice(proportionsIndex, 1);
            }

            if (array && array.length) {
                for (var i = 0; i < array.length; i++) {
                    if (isProportionsSelected(vm.selectedManufacturer.proportionsArray[i])) {
                        isFindedSelected = true;
                        break;
                    }
                }
            }

            if (!isFindedSelected) {
                vm.resultMaterial.proportions = null;
            }

            onPurposeChanged();
        }

        function decrementNf(proportions) {
            proportions.nf_extra = ((proportions.nf_extra || 0) * 10 - 1) / 10;
            onPurposeChanged();
        }

        function incrementNf(proportions) {
            proportions.nf_extra = ((proportions.nf_extra || 0) * 10 + 1) / 10;
            onPurposeChanged();
        }

        function onProportionsChange(proportions) {
            proportions.length = proportions.length > 0 ? proportions.length : 1;
            proportions.width = proportions.width > 0 ? proportions.width : 1;
            proportions.height = proportions.height > 0 ? proportions.height : 1;
            proportions.label = proportions.label || '';
            proportions.nf = commonService.calcNf(proportions);
            calcResultDepth();
        }

        function onPurposeChanged() {
            $scope.onPurposeChanged({purpose: vm.commonMaterialInfo.purpose});
        }

        function onMasonryTypesChanged() {
            $scope.onMasonryTypesChanged({masonryTypes: vm.masonryTypes});
            selectMasonryTypeIfSingle();
        }

        function toggleMasonryTypeEditMode(masonryTypeIndex) {
            if (vm.editedMasonryTypeIndex === masonryTypeIndex) {
                vm.editedMasonryTypeIndex = null;
                onMasonryTypesChanged();
            } else {
                vm.editedMasonryTypeIndex = masonryTypeIndex;
            }
        }

        function removeMasonryType(masonryTypeIndex) {
            vm.masonryTypes.splice(masonryTypeIndex, 1);
            vm.editedMasonryTypeIndex = null;
            onMasonryTypesChanged();
        }

        function getClassForProportionsOrderCheckbox(key) {
            var orderKeyInfo = getProportionsOrderInfoByKey(key);

            if (!orderKeyInfo) {
                return $scope.isEditMode ? 'glyphicon-sort-by-attributes text-light' : '';
            }

            return orderKeyInfo.isReverse
                ? 'glyphicon-sort-by-attributes-alt proportions-filter-count-' + orderKeyInfo.index
                : 'glyphicon-sort-by-attributes proportions-filter-count-' + orderKeyInfo.index;
        }

        function setProportionsOrder(key) {
            if (!$scope.isEditMode) {
                return;
            }

            var orderKeyInfo = getProportionsOrderInfoByKey(key);

            if (!orderKeyInfo) {
                vm.commonMaterialInfo.purpose.data.orderKeysInfo.push({
                    key: key,
                    isReverse: false,
                    index: vm.commonMaterialInfo.purpose.data.orderKeysInfo.length
                });
            } else {
                orderKeyInfo.isReverse = !orderKeyInfo.isReverse;
            }

            sortProportions();
            onPurposeChanged();
        }

        function getProportionsOrderInfoByKey(key) {
            var orderKeysInfo = vm.commonMaterialInfo.purpose.data.orderKeysInfo;

            for (var i = 0; i < orderKeysInfo.length; i++) {
                if (orderKeysInfo[i].key === key) {
                    return orderKeysInfo[i];
                }
            }
        }

        function sortProportions() {
            var orderKeysInfo = vm.commonMaterialInfo.purpose.data.orderKeysInfo;

            if (!orderKeysInfo || !orderKeysInfo.length) {
                return;
            }

            vm.selectedManufacturer.proportionsArray.sort(function (a, b) {
                for (var i = 0; i < orderKeysInfo.length; i++) {
                    var key = orderKeysInfo[i].key;

                    if (a[key] !== b[key]) {
                        var result = orderKeysInfo[i].isReverse
                            ? a[key] < b[key]
                            : a[key] > b[key];

                        return result ? 1 : -1;
                    }
                }

                return 0;
            });
        }

        function clearProportionsOrderForKey(key) {
            var orderKeysInfo = vm.commonMaterialInfo.purpose.data.orderKeysInfo;
            var wasDeleted = false;

            if (!orderKeysInfo || !orderKeysInfo.length) {
                return;
            }

            for (var i = 0; i < orderKeysInfo.length; i++) {
                if (!wasDeleted && orderKeysInfo[i].key === key) {
                    orderKeysInfo.splice(i, 1);
                    wasDeleted = true;
                    i--;
                } else {
                    orderKeysInfo[i].index = i;
                }
            }

            sortProportions();
            onPurposeChanged();
        }

        function addManufacturer() {
            vm.commonMaterialInfo.purpose.data.manufacturers.push({
                label: 'Производитель',
                proportionsArray: []
            });
            onPurposeChanged();
        }

        function toggleManufacturerEditMode(isNeedRestoreBackup) {
            var purposeData = vm.commonMaterialInfo.purpose.data;

            if (!vm.isManufacturerEditing) {
                manufacturersBackup = angular.copy(purposeData.manufacturers);
            }

            vm.isManufacturerEditing = !vm.isManufacturerEditing;

            if (isNeedRestoreBackup) {
                purposeData.manufacturers = manufacturersBackup;
                vm.selectedManufacturerIndex = purposeData.defaultManufacturerIndex;
                vm.selectedManufacturer = purposeData.manufacturers[vm.selectedManufacturerIndex];
            }

            onPurposeChanged();
        }

        function selectManufacturer(manufacturer, manufacturerIndex) {
            vm.selectedManufacturer = manufacturer;
            vm.selectedManufacturerIndex = manufacturerIndex;
            vm.resultMaterial.proportions = null;
            selectMasonryTypeIfSingle();
            calcResultDepth();
        }

        function removeManufacturer(manufacturerIndex) {
            var purposeData = vm.commonMaterialInfo.purpose.data;

            purposeData.manufacturers.splice(manufacturerIndex, 1);

            if (purposeData.defaultManufacturerIndex >= purposeData.manufacturers.length) {
                purposeData.defaultManufacturerIndex = 0;
            }

            vm.selectedManufacturerIndex = purposeData.defaultManufacturerIndex;
            vm.selectedManufacturer = purposeData.manufacturers[vm.selectedManufacturerIndex];
            onPurposeChanged();
        }

        function isDefaultManufacturer(manufacturerIndex) {
            return vm.commonMaterialInfo.purpose.data.defaultManufacturerIndex === manufacturerIndex;
        }

        function setDefaultManufacturer(manufacturerIndex) {
            vm.commonMaterialInfo.purpose.data.defaultManufacturerIndex = manufacturerIndex;
            onPurposeChanged();
        }

        function moveLeftMasonryType(masonryTypeIndex) {
            commonService.swapInTheArray(vm.masonryTypes, masonryTypeIndex, masonryTypeIndex - 1);
            onMasonryTypesChanged();
        }

        function moveRightMasonryType(masonryTypeIndex) {
            commonService.swapInTheArray(vm.masonryTypes, masonryTypeIndex, masonryTypeIndex + 1);
            onMasonryTypesChanged();
        }

        function onCustomProportionsModeChange() {
            vm.isCustomProportionsMode = !vm.isCustomProportionsMode;

            if (!vm.isCustomProportionsMode) {
                var allProportions = vm.selectedManufacturer.proportionsArray;

                for (var i = 0; i < allProportions.length; i++) {
                    if (vm.isProportionsSelected(allProportions[i])) {
                        return;
                    }
                }

                vm.resultMaterial.proportions = null;
                calcResultDepth();
            } else {
                vm.resultMaterial.proportions = vm.resultMaterial.proportions
                    ? angular.copy(vm.resultMaterial.proportions)
                    : angular.copy(DEFAULT_NEW_PROPORTIONS);
            }
        }

        function getPrintedNf(proportions) {
            return ((proportions.nf || 0) * 10 + (proportions.nf_extra || 0) * 10) / 10;
        }

        function moveManufacturerUp(index) {
            commonService.swapInTheArray(vm.commonMaterialInfo.purpose.data.manufacturers, index - 1, index);
            onPurposeChanged();
        }

        function moveManufacturerDown(index) {
            commonService.swapInTheArray(vm.commonMaterialInfo.purpose.data.manufacturers, index, index + 1);
            onPurposeChanged();
        }

        function isVisibleColumn(key) {
            if (!vm.selectedManufacturer.hiddenColumns) {
                vm.selectedManufacturer.hiddenColumns = {};

                return true;
            }

            return $scope.isEditMode || !vm.selectedManufacturer.hiddenColumns[key];
        }

        function toggleVisibleColumn(key) {
            if (!vm.selectedManufacturer.hiddenColumns) {
                vm.selectedManufacturer.hiddenColumns = {};
            }

            vm.selectedManufacturer.hiddenColumns[key] = !vm.selectedManufacturer.hiddenColumns[key];
            hideSameRows();
            onPurposeChanged();
        }

        function hideSameRows() {
            var proportions = vm.selectedManufacturer.proportionsArray;
            var map = {};
            var visibleColumns = ['length', 'width', 'height', 'label'].filter(function (key) {
                return !vm.selectedManufacturer.hiddenColumns || !vm.selectedManufacturer.hiddenColumns[key];
            });

            if (!proportions || !proportions.length) {
                return;
            }

            proportions.forEach(function (value) {
                var key = visibleColumns
                    .map(function (key) {
                        return value[key] ? value[key].toString() : '';
                    })
                    .join('');

                value.hidden = map[key];
                map[key] = true;
            });
        }

        function isVisibleMasonryType(masonryType) {
            if (!masonryType.hiddenIn) {
                return true;
            }

            return !masonryType.hiddenIn[vm.selectedManufacturer.label];
        }

        function toggleVisibleMasonryType(masonryType) {
            if (!masonryType.hiddenIn) {
                masonryType.hiddenIn = {};
            }

            masonryType.hiddenIn[vm.selectedManufacturer.label] = !masonryType.hiddenIn[vm.selectedManufacturer.label];
            onMasonryTypesChanged();
            selectMasonryTypeIfSingle();
        }
    }
}
