angular.module('calc')
    .directive('materialSelector', materialSelector);

materialSelector.$inject = ['$http', 'commonService', '$timeout'];

function materialSelector($http, commonService, $timeout) {
    return {
        restrict: 'E',
        scope: {
            selectedMaterials: '<',
            selectingMaterialId: '=',
            materials: '=',
            resultMaterial: '=',
            multilayer: '=',
            onSelect: '&',
            onClear: '&',
            changeLayer: '&'
        },
        templateUrl: 'calc_js/materialSelector/materialSelector.template.html',
        controller: materialSelectorController,
        controllerAs: '$ctrl'
    };

    function materialSelectorController($scope) {
        var vm = this;
        var STEPS_ENUM = {
            COMMON_INFO_STEP: 'COMMON_INFO_STEP',
            FINISH_STEP: 'FINISH_STEP'
        };
        var STEPS = [STEPS_ENUM.COMMON_INFO_STEP, STEPS_ENUM.FINISH_STEP];
        var COLORS = ['#5bc0de', '#5cb85c', '#f0ad4e', '#d9534f', '#337ab7'];

        var gostsBackup;
        var materialsDatabaseBackup;

        vm.clear = clear;
        vm.showNextStep = showNextStep;
        vm.showPreviousStep = showPreviousStep;
        vm.isMainFieldsFilled = isMainFieldsFilled;
        vm.isFieldFilled = isFieldFilled;
        vm.addMaterialGroup = addMaterialGroup;
        vm.selectMaterialGroupItem = selectMaterialGroupItem;
        vm.isMaterialGroupItemSelected = isMaterialGroupItemSelected;
        vm.cloneMaterialGroupItem = cloneMaterialGroupItem;
        vm.removeMaterialGroup = removeMaterialGroup;
        vm.removeMaterialGroupItem = removeMaterialGroupItem;
        vm.moveMaterialGroupUp = moveMaterialGroupUp;
        vm.moveMaterialGroupDown = moveMaterialGroupDown;
        vm.moveMaterialGroupItemUp = moveMaterialGroupItemUp;
        vm.moveMaterialGroupItemDown = moveMaterialGroupItemDown;
        vm.enableEditMode = enableEditMode;
        vm.disableEditMode = disableEditMode;
        vm.removeField = removeField;
        vm.addField = addField;
        vm.moveLeftField = moveLeftField;
        vm.moveRightField = moveRightField;
        vm.addFieldItem = addFieldItem;
        vm.selectFieldItem = selectFieldItem;
        vm.isFieldItemSelected = isFieldItemSelected;
        vm.moveLeftFieldItem = moveLeftFieldItem;
        vm.moveRightFieldItem = moveRightFieldItem;
        vm.removeFieldItem = removeFieldItem;
        vm.onFinishStepMasonryTypesChanged = onFinishStepMasonryTypesChanged;
        vm.getMasonryTypes = getMasonryTypes;
        vm.onFinishStepPurposeChanged = onFinishStepPurposeChanged;
        vm.getPriceMaterialName = getPriceMaterialName;
        vm.getPriceMaterialColor = getPriceMaterialColor;
        vm.getPriceMaterialById = getPriceMaterialById;
        vm.selectPriceMaterialForFieldItem = selectPriceMaterialForFieldItem;
        vm.selectPriceMaterialForGroupItem = selectPriceMaterialForGroupItem;
        vm.toggleGostEditPanelVisibility = toggleGostEditPanelVisibility;
        vm.addGost = addGost;
        vm.removeGost = removeGost;
        vm.fieldDescriptionOrderDecrement = fieldDescriptionOrderDecrement;
        vm.fieldDescriptionOrderIncrement = fieldDescriptionOrderIncrement;
        vm.onFactureSelected = onFactureSelected;
        vm.isSelectedMaterialButtonVisible = isSelectedMaterialButtonVisible;
        vm.changeLayer = changeLayer;
        vm.prevendDefaultIfEditMode = prevendDefaultIfEditMode;
        vm.addGroupItem = addGroupItem;
        vm.disableMultilayer = disableMultilayer;
        vm.STEPS_ENUM = STEPS_ENUM;
        vm.currentStepIndex = 0;
        vm.currentStep = STEPS[vm.currentStepIndex];
        vm.requiredCommonFields = {
            purpose: 'purpose'
        };
        vm.notDeletableFields = {
            purpose: 'purpose',
            aggregate: 'aggregate'
        };
        vm.materialGroups = [];
        vm.selectedMaterialGroupItem = null;
        vm.isLoading = true;
        vm.resultMaterial = angular.copy($scope.resultMaterial);
        vm.isUserCanEdit = window.isUserCanEdit;
        vm.isEditMode = false;
        vm.isFinishStepFulfilled = false;
        vm.priceMaterialColors = [];
        vm.isGostEditPanelVisible = false;
        vm.materialsDatabase = null;

        $scope.common = commonService;

        init();

        function init() {
            function getFromLocalstorage() {
                var materialGroupsStringed = localStorage.getItem('materials-database');
                var materialGroups = materialGroupsStringed && angular.fromJson(materialGroupsStringed);

                return materialGroups || [];
            }

            $http.get('/mock/materials-database.json')
            //$http.get('https://kubdem.ru/calcdev/materials')
                .then(function (responce) {
                    if (responce.data) {
                        // TODO: Local
                        var dataFromLocalStorage = getFromLocalstorage();

                        responce.data = dataFromLocalStorage.length
                            ? dataFromLocalStorage
                            : responce.data;

                        // TODO: Release
                        materialsDatabaseBackup = responce.data[0].item;
                        resetMaterialsDatabase();
                    } else {
                        cancel();
                    }
                })
                .catch(cancel)
                .finally(function () {
                    vm.isLoading = false;
                });
        }

        function resetMaterialsDatabase() {
            vm.materialsDatabase = angular.copy(materialsDatabaseBackup);
            vm.materialGroups = vm.materialsDatabase.materialsGroups;

            initMaterialGroups();
            initMaterialGroup();
            initPriceMaterialColors();
        }

        function enableEditMode() {
            vm.isEditMode = true;
        }

        function disableEditMode(needSave) {
            if (needSave) {
                vm.isEditMode = false;
                vm.isGostEditPanelVisible = false;
                localStorage.setItem('materials-database', angular.toJson([{item: vm.materialsDatabase}]));
                materialsDatabaseBackup = angular.copy(vm.materialsDatabase);

                $http.post('https://kubdem.ru/calcdev/savematerials', {items: [vm.materialsDatabase]})
                    .then(function (responce) {
                    });
            } else if (confirm('Все изменения будут потеряны!')) {
                vm.isEditMode = false;
                vm.isGostEditPanelVisible = false;
                resetMaterialsDatabase();
            }
        }

        function initMaterialGroups() {
            for (var groupIndex = 0; groupIndex < vm.materialGroups.length; groupIndex++) {
                var group = vm.materialGroups[groupIndex];

                group.isOpen = false;

                for (var groupItemIndex = 0; groupItemIndex < group.items.length; groupItemIndex++) {
                    var groupItem = group.items[groupItemIndex];

                    if (groupItem.isMultilayer) {
                        continue;
                    }

                    if (!groupItem.proportionsLabels || !groupItem.proportionsLabels.length) {
                        groupItem.proportionsLabels = [
                            {name: 'одинарный'},
                            {name: 'утолщенный'},
                            {name: 'евро'}
                        ];
                    }

                    if (!groupItem.facture) {
                        groupItem.facture = commonService.ALL_FACTURES[0];
                    }

                    for (var fieldIndex = 0; fieldIndex < groupItem.fields.length; fieldIndex++) {
                        var field = groupItem.fields[fieldIndex];

                        for (var fieldItemIndex = 0; fieldItemIndex < field.items.length; fieldItemIndex++) {
                            var fieldItem = field.items[fieldItemIndex];

                            if (fieldItem) {
                                fieldItem.fieldLabel = field.label;

                                if (field.key === 'purpose') {
                                    updateProportions(fieldItem);
                                }

                                initPriceMaterial(groupItem, field, fieldItem);
                            }
                        }
                    }
                }
            }
        }

        function updateProportions(fieldItem) {
            // TODO убрать!!!! Сейчас для перегона старых материалов в новый формат
            if (isProportionsArray(fieldItem.data)) {
                fieldItem.data = {
                    proportionsArray: fieldItem.data
                };
            }
            if (fieldItem.data.proportionsArray) {
                fieldItem.data = {
                    manufacturers: [{
                        label: 'Производитель',
                        proportionsArray: fieldItem.data.proportionsArray
                    }],
                    defaultManufacturerIndex: 0,
                    orderKey: fieldItem.data.orderKey,
                    isOrderReverse: fieldItem.data.isOrderReverse
                };
            }

            if (!fieldItem.data || !fieldItem.data.manufacturers) {
                fieldItem.data = {
                    manufacturers: [
                        {
                            label: 'Производитель',
                            proportionsArray: []
                        }
                    ],
                    defaultManufacturerIndex: 0
                };
            }

            var manufacturers = fieldItem.data.manufacturers;

            for (var manufacturerIndex = 0; manufacturerIndex < manufacturers.length; manufacturerIndex++) {
                var proportionsArray = manufacturers[manufacturerIndex].proportionsArray;
                var hiddenColumns = manufacturers[manufacturerIndex].hiddenColumns;

                for (var proportionIndex = 0; proportionIndex < proportionsArray.length; proportionIndex++) {
                    var proportions = proportionsArray[proportionIndex];

                    proportions.length = proportions.length > 0 ? proportions.length : 1;
                    proportions.width = proportions.width > 0 ? proportions.width : 1;
                    proportions.height = proportions.height > 0 ? proportions.height : 1;
                    proportions.label = proportions.label || '';
                    proportions.nf = commonService.calcNf(proportions);
                    proportions.nf_extra = proportions.nf_extra || 0;
                }
            }
        }

        function isProportionsArray(value) {
            return value && value.length && (value[0].length || value[0].width || value[0].height || value[0].nf);
        }

        function initPriceMaterial(group, field, fieldItem) {
            if (field.isCanContainMaterial) {
                fieldItem.data.materialId = fieldItem.data.materialId || group.materialId || 1;
            }
        }

        function initPriceMaterialColors() {
            for (var i = 0; i < $scope.materials.length; i++) {
                vm.priceMaterialColors[$scope.materials[i].id] = COLORS[i];
            }
        }

        function initMaterialGroup() {
            if ($scope.resultMaterial && $scope.resultMaterial.materialGroupName) {
                for (var groupIndex = 0; groupIndex < vm.materialGroups.length; groupIndex++) {
                    var group = vm.materialGroups[groupIndex];

                    for (var groupItemIndex = 0; groupItemIndex < group.items.length; groupItemIndex++) {
                        var groupItem = group.items[groupItemIndex];

                        if (groupItem.name === $scope.resultMaterial.materialGroupName) {
                            selectMaterialGroupItem(groupItem, true);
                            return;
                        }
                    }
                }
            }

            toInitialState();
        }

        function toInitialState() {
            vm.materialGroups.forEach(function (group) {
                group.isOpen = false;
            });
            vm.selectedMaterialGroupItem = null;
            vm.resultMaterial = {
                commonMaterialInfo: {}
            };
            vm.currentStepIndex = 0;
            vm.currentStep = STEPS[vm.currentStepIndex];
        }

        function cancel() {
            $scope.onSelect({materialGroup: $scope.resultMaterial});
        }

        function clear() {
            if (confirm('Очистить?')) {
                $scope.onClear();
            }
        }

        function showNextStep() {
            if (vm.currentStepIndex === STEPS.length - 1) {
                vm.resultMaterial.descriptionKeys = getDescriptionKeys();
                vm.resultMaterial.facture = vm.selectedMaterialGroupItem.facture;
                $scope.onSelect({materialGroup: vm.resultMaterial});
            }

            vm.currentStepIndex++;
            vm.currentStep = STEPS[vm.currentStepIndex];


        }

        function getDescriptionKeys() {
            return vm.selectedMaterialGroupItem.fields
                .reduce(function (result, value) {
                    if (value.isUsedForDescription) {
                        result.push({
                            index: value.descriptionOrder,
                            key: value.key
                        });
                    }

                    return result;
                }, [])
                .sort(function (a, b) {
                    return a.index - b.index;
                })
                .map(function (value) {
                    return value.key;
                });
        }

        function showPreviousStep() {
            if (vm.currentStepIndex === 0) {
                cancel();
            }

            vm.currentStepIndex--;
            vm.currentStep = STEPS[vm.currentStepIndex];
        }

        function isMainFieldsFilled() {
            if (vm.currentStep === STEPS_ENUM.COMMON_INFO_STEP) {
                for (key in vm.requiredCommonFields) {
                    if (vm.requiredCommonFields.hasOwnProperty(key)
                        && !vm.resultMaterial.commonMaterialInfo[vm.requiredCommonFields[key]]) {
                        return false;
                    }
                }
            } else if (vm.currentStep === STEPS_ENUM.FINISH_STEP) {
                return vm.isFinishStepFulfilled;
            }

            return true;
        }

        function isFieldFilled(field) {
            return !!vm.resultMaterial.commonMaterialInfo[field.key];
        }

        function addMaterialGroup() {
            vm.materialGroups.push({
                groupName: 'Название',
                items: []
            })
        }

        function selectMaterialGroupItem(groupItem, isNeedSafeRezultMaterial) {
            if (vm.isEditMode) {
                return;
            }

            if (groupItem.isMultilayer) {
                $scope.multilayer.isMultilayer = true;
                $scope.multilayer.multilayerText = groupItem.name;

                return;
            }

            vm.selectedMaterialGroupItem = groupItem;

            if (!isNeedSafeRezultMaterial || !vm.resultMaterial) {
                vm.resultMaterial = {
                    commonMaterialInfo: {}
                };
            }

            vm.resultMaterial.materialGroupName = vm.selectedMaterialGroupItem.name;
            vm.resultMaterial.materialId = vm.selectedMaterialGroupItem.materialId;
            vm.currentStepIndex = 0;
            vm.currentStep = STEPS[vm.currentStepIndex];
        }

        function isMaterialGroupItemSelected(groupItem, checkIfEditMode) {
            if (!checkIfEditMode && vm.isEditMode) {
                return false;
            }

            return vm.selectedMaterialGroupItem === groupItem;
        }

        function cloneMaterialGroupItem(group, groupItem) {
            group.items.push(angular.copy(groupItem));
        }

        function removeMaterialGroup(groupIndex, event) {
            prevendDefaultIfEditMode(event);

            if (!confirm('Удалить группу?')) {
                return;
            }

            vm.materialGroups[groupIndex].items.forEach(function (groupItem, index) {
                removeMaterialGroupItem(vm.materialGroups[groupIndex], index, true);
            });

            vm.materialGroups.splice(groupIndex, 1);
        }

        function removeMaterialGroupItem(group, index, silent) {
            var itWasSelected = isMaterialGroupItemSelected(group.items[index], true);

            if (silent || confirm('Удалить материал?')) {
                group.items.splice(index, 1);

                if (itWasSelected) {
                    vm.selectedMaterialGroupItem = null;
                }
            }
        }

        function moveMaterialGroupUp(groupIndex) {
            commonService.swapInTheArray(vm.materialGroups, groupIndex - 1, groupIndex);
        }

        function moveMaterialGroupDown(groupIndex) {
            commonService.swapInTheArray(vm.materialGroups, groupIndex + 1, groupIndex);
        }

        function moveMaterialGroupItemUp(groupIndex, index) {
            var group = vm.materialGroups[groupIndex];

            if (!index) {
                vm.materialGroups[groupIndex - 1].items.push(group.items.shift());

                return;
            }

            commonService.swapInTheArray(group.items, index - 1, index);
        }

        function moveMaterialGroupItemDown(groupIndex, index) {
            var group = vm.materialGroups[groupIndex];

            if (index === (group.items.length - 1)) {
                vm.materialGroups[groupIndex + 1].items.unshift(group.items.pop());

                return;
            }

            commonService.swapInTheArray(group.items, index + 1, index);
        }

        function removeField(fieldIndex) {
            vm.selectedMaterialGroupItem.fields.splice(fieldIndex, 1);
        }

        function addField(isCanContainMaterial) {
            var fieldKey = getNewFieldKey('field');

            vm.selectedMaterialGroupItem.fields.push({
                key: fieldKey,
                label: 'Название',
                isUsedForDescription: false,
                descriptionOrder: 1,
                isCanContainMaterial: isCanContainMaterial,
                items: []
            });
            addFieldItem(vm.selectedMaterialGroupItem.fields[vm.selectedMaterialGroupItem.fields.length - 1]);
        }

        function getNewFieldKey(prefix) {
            var allFieldsKeys = vm.selectedMaterialGroupItem.fields.reduce(function (result, field) {
                result[field.key] = true;

                return result;
            }, {});

            for (var key = 0; key < 100; key++) {
                if (!allFieldsKeys[prefix + key]) {
                    return prefix + key;
                }
            }

            return prefix;
        }

        function moveLeftField(fieldIndex) {
            commonService.swapInTheArray(vm.selectedMaterialGroupItem.fields, fieldIndex, fieldIndex - 1);
        }

        function moveRightField(fieldIndex) {
            commonService.swapInTheArray(vm.selectedMaterialGroupItem.fields, fieldIndex, fieldIndex + 1);
        }

        function addFieldItem(field) {
            var data = {};

            if (field.isCanContainMaterial) {
                data.materialId = vm.selectedMaterialGroupItem.materialId;
            }

            if (field.key === 'purpose') {
                data.manufacturers = [{
                    label: 'Производитель',
                    proportionsArray: []
                }];
                data.defaultManufacturerIndex = 0;
            }


            field.items.push({
                label: 'название',
                fieldLabel: field.label,
                data: data
            });
        }

        function selectFieldItem(fieldItem, fieldKey) {
            if (vm.isEditMode) {
                return;
            }

            vm.resultMaterial.commonMaterialInfo[fieldKey] = isFieldItemSelected(fieldItem, fieldKey)
                ? null
                : fieldItem;
        }

        function isFieldItemSelected(fieldItem, fieldKey) {
            if (vm.isEditMode) {
                return false;
            }

            return vm.resultMaterial.commonMaterialInfo[fieldKey]
                && vm.resultMaterial.commonMaterialInfo[fieldKey].label === fieldItem.label;
        }

        function moveLeftFieldItem(fieldItem, fieldItemIndex) {
            commonService.swapInTheArray(fieldItem.items, fieldItemIndex, fieldItemIndex - 1);
        }

        function moveRightFieldItem(fieldItem, fieldItemIndex) {
            commonService.swapInTheArray(fieldItem.items, fieldItemIndex, fieldItemIndex + 1);
        }

        function removeFieldItem(fieldItem, fieldItemIndex) {
            fieldItem.items.splice(fieldItemIndex, 1);
        }

        function onFinishStepMasonryTypesChanged(masonryTypes) {
            if (!vm.selectedMaterialGroupItem.masonryTypesForPurpose) {
                vm.selectedMaterialGroupItem.masonryTypesForPurpose = {
                    default: vm.selectedMaterialGroupItem.masonryTypes
                };
                vm.selectedMaterialGroupItem.masonryTypes = undefined;
            }

            var selectedPurposeLabel = vm.resultMaterial.commonMaterialInfo.purpose.label;

            vm.selectedMaterialGroupItem.masonryTypesForPurpose[selectedPurposeLabel] = angular.copy(masonryTypes);
        }

        function getMasonryTypes() {
            var selectedPurposeLabel = vm.resultMaterial.commonMaterialInfo.purpose.label;

            if (!vm.selectedMaterialGroupItem.masonryTypesForPurpose) {
                return vm.selectedMaterialGroupItem.masonryTypes;
            }

            return vm.selectedMaterialGroupItem.masonryTypesForPurpose[selectedPurposeLabel]
                || vm.selectedMaterialGroupItem.masonryTypesForPurpose.default;
        }

        function onFinishStepPurposeChanged(purpose) {
            for (var fieldIndex = 0; fieldIndex < vm.selectedMaterialGroupItem.fields.length; fieldIndex++) {
                var field = vm.selectedMaterialGroupItem.fields[fieldIndex];

                if (field.label === purpose.fieldLabel) {
                    for (var fieldItemIndex = 0; fieldItemIndex < field.items.length; fieldItemIndex++) {
                        var fieldItem = field.items[fieldItemIndex];

                        if (fieldItem.label === purpose.label) {
                            fieldItem.data = purpose.data;
                            return;
                        }
                    }
                }
            }
        }

        function getPriceMaterialName(materialId) {
            return getPriceMaterialById(materialId).name;
        }

        function getPriceMaterialColor(materialId) {
            return vm.priceMaterialColors[materialId];
        }

        function getPriceMaterialById(materialId) {
            var index = materialId - 1;

            if ($scope.materials[index].id === materialId) {
                return $scope.materials[index];
            }

            for (var i = 0; i < $scope.materials.length; i++) {
                if ($scope.materials[i].id === materialId) {
                    return $scope.materials[i];
                }
            }
        }

        function selectPriceMaterialForFieldItem(fieldItem, materialId) {
            fieldItem.data.materialId = materialId;
        }

        function selectPriceMaterialForGroupItem(groupItem, materialId) {
            groupItem.materialId = materialId;
        }

        function toggleGostEditPanelVisibility(isNeedRestoreBackup) {
            if (!vm.isGostEditPanelVisible) {
                gostsBackup = angular.copy(vm.selectedMaterialGroupItem.gosts);
            }

            vm.isGostEditPanelVisible = !vm.isGostEditPanelVisible;


            if (isNeedRestoreBackup) {
                vm.selectedMaterialGroupItem.gosts = gostsBackup;
            }
        }

        function addGost() {
            vm.selectedMaterialGroupItem.gosts.push({
                label: 'GOST NAME',
                link: ''
            });
        }

        function removeGost(gostIndex) {
            vm.selectedMaterialGroupItem.gosts.splice(gostIndex, 1);
        }

        function fieldDescriptionOrderDecrement(field) {
            if (field.descriptionOrder > 1) {
                field.descriptionOrder--;
            }
        }

        function fieldDescriptionOrderIncrement(field) {
            field.descriptionOrder++;
        }

        function onFactureSelected(materialGroup, facture) {
            materialGroup.facture = facture;
        }

        function isSelectedMaterialButtonVisible(key) {
            if (commonService.ALWAYS_SHOWED_BUTTONS_FOR_MATERIAL_SELECTING[key]) {
                return true;
            }

            return $scope.selectedMaterials[key] || $scope.selectedMaterials[commonService.getPrevLayerKey(key)];
        }

        function changeLayer(key) {
            if (vm.isEditMode) {
                return;
            }

            $scope.changeLayer({key: key});
            $timeout(function () {
                if ($scope.resultMaterial) {
                    vm.resultMaterial = angular.copy($scope.resultMaterial);
                    initMaterialGroup();
                } else {
                    toInitialState();
                }
            });
        }

        function prevendDefaultIfEditMode(event) {
            if (vm.isEditMode) {
                event.preventDefault();
                event.stopImmediatePropagation();
            }
        }

        function addGroupItem(group, isMultilayer) {
            if (isMultilayer) {
                group.items.push({
                    name: 'Название',
                    isMultilayer: true
                });

                return;
            }

            group.items.push({
                name: 'Название',
                gosts: [],
                fields: [{
                    key: 'purpose',
                    label: 'Назначение',
                    isUsedForDescription: true,
                    items: []
                }],
                masonryTypes: [],
                materialId: 1,
                proportionsLabels: [{
                    name: 'Название'
                }]
            });
        }

        function disableMultilayer() {
            $scope.multilayer.isMultilayer = false;
            $scope.multilayer.multilayerText = '';
        }
    }
}
