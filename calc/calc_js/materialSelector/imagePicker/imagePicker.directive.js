angular.module('calc')
    .directive('imagePicker', imagePicker);

imagePicker.$inject = ['commonService'];

function imagePicker(commonService) {
    return {
        restrict: 'E',
        scope: {
            facture: '=',
            height: '=',
            isSelectable: '=',
            onSelect: '&'
        },
        templateUrl: 'calc_js/materialSelector/imagePicker/imagePicker.template.html',
        controller: imagePickerController,
        controllerAs: '$ctrl'
    };

    function imagePickerController($scope) {
        var vm = this;

        $scope.height = $scope.height || 20;

        vm.onSelect = onSelect;
        vm.isStrokedHatch = isStrokedHatch;
        vm.factures = commonService.ALL_FACTURES;
        vm.facture = $scope.facture || vm.factures[0];
        vm.radius = $scope.height / 2;
        vm.diameter = $scope.height;
        vm.containerStyle = {
            width: vm.diameter + 'px',
            height: vm.diameter + 'px'
        };
        vm.numRows = 0;
        // чтобы верно работало подправить в main.css для image-picker-panel-selectable:hover
        // значение width на (numPerRow + 1) * 100%
        vm.numPerRow = 10;
        vm.rows = [];
        vm.FACTURES_LABELS = commonService.FACTURES_LABELS;

        init();

        function init() {
            reorganizeFacturesArray();
            // На тот случай, если на старте не было выбрано.
            onSelect(vm.facture);
        }

        function onSelect(facture) {
            if (facture === vm.facture) {
                return;
            }

            vm.facture = facture;
            reorganizeFacturesArray();

            if ($scope.onSelect) {
                $scope.onSelect({facture: facture});
            }
        }

        function reorganizeFacturesArray() {
            vm.factures = commonService.ALL_FACTURES.filter(function (facture) { return facture !== vm.facture; });
            vm.numRows = Math.ceil(vm.factures.length / vm.numPerRow);
            vm.rows = [];

            for (var i = 0; i < vm.numRows; i++) {
                vm.rows.push(vm.factures.slice(i * vm.numPerRow, (i + 1) * vm.numPerRow));
            }
        }

        function isStrokedHatch(facture) {
            return commonService.isStrokedHatch(facture);
        }
    }
}
