angular.module('calc')
    .controller('printCtrl', printController);

printController.$inject = ['$rootScope', '$scope', '$location', 'commonService', 'currentPricesService',
    'usedFactorsService'];

function printController($rootScope, $scope, $location, common, currentPrices, usedFactorsService) {
    var options = currentPrices.options;

    // Сервисы
    $scope.common = common;
    $scope.usedFactorsService = usedFactorsService;
    $scope.currentPrices = currentPrices;

    // Методы
    $scope.ifNds = ifNds;
    $scope.addSection = addSection;
    $scope.delSection = delSection;
    $scope.addItem = addItem;
    $scope.delItem = delItem;

    usedFactorsService.updateUsedFactors();

    if (!$rootScope.sections) {
        $location.path('/');
        return;
    }

    if (!$rootScope.nds) {
        $rootScope.nds = false;
    }

    $rootScope.isSearchBarVisible = false;

    function ifNds() {
        return $rootScope.nds;
    }

    function addSection() {
        $location.path('/addSection');
    }

    function delSection(key) {
        if (!confirm('Удалить раздел?')) {
            return;
        }
        $rootScope.sections.splice(key, 1);

        if ($rootScope.sections.length === 0) {
            $rootScope.sections.itogo = 0;
            $rootScope.sections.discount = 0;
            $rootScope.sections.clientDiscount = 0;
            $rootScope.sections.isClientDiscount = false;
        }
        $rootScope.isEditSections = common.isEditSections();
        common.itogo();
    }

    function addItem(key) {
        $rootScope.curentSection = key;
        $location.path('/addItem');
    }

    function delItem(key, k) {
        if (!confirm('Удалить услугу?')) {
            return;
        }
        $rootScope.sections[key]['items'].splice(k, 1);

        common.itogo();
        $rootScope.isEditSections = common.isEditSections();
    }
}
