angular.module('calc')
    .factory('focus', focusFactory);

focusFactory.$inject = ['$timeout', '$window'];

function focusFactory($timeout, $window) {
    return function (id) {
        $timeout(function () {
            var element = $window.document.getElementById(id);
            if (element)
                element.focus();
        });
    };
}
