angular.module('calc')
    .factory('sectionsUpgraderService', sectionsUpgraderService);

sectionsUpgraderService.$inject = ['currentPricesService', 'commonService'];

function sectionsUpgraderService(currentPrices, common) {
    var service = {
        upgradeSections: upgradeSections
    };

    return service;

    function upgradeSections(sections) {
        if (!sections || sections.length === 0) {
            return [];
        }

        for (var sectionIndex = 0; sectionIndex < sections.length; sectionIndex++) {
            var items = sections[sectionIndex].items;

            if (items) {
                for (var itemIndex = 0; itemIndex < items.length; itemIndex++) {
                    items[itemIndex] = upgradeItem(items[itemIndex]);
                }
            }
        }

        return sections;
    }

    function upgradeItem(item) {
        var functionsByWork = [upgradeItemType1, upgradeItemType2, upgradeItemType3, upgradeItemType4];

        return functionsByWork[item.work - 1](item);
    }

    function upgradeItemType1(item) {
        return {
            work: item.work,
            itemName: item.itemName,
            plos: item.plos,
            comment: item.comment,
            material: item.material,
            multilayer: item.multilayer,
            selectedMaterials: item.selectedMaterials,
            diameterName: item.diameterName || item.diameter,
            diameterPrice: item.diameterPrice || currentPrices.priceDiameter(item.diameter, 1),
            glubina: tryConvertToNumber(item.glubina),
            kolvo: tryConvertToNumber(item.kolvo),
            minSum: tryConvertToNumber(item.minSum || 0),
            vars: getUpgradedVars(item.vars, item),
            sumItem: tryConvertToNumber(item.sumItem),
            sumItogo: tryConvertToNumber(item.sumItogo),
            sum: tryConvertToNumber(item.sum),
            priceStyle: item.priceStyle
        };
    }

    function upgradeItemType2(item) {
        return {
            work: item.work,
            itemName: item.itemName,
            work_2: item.work_2,
            plos: item.plos,
            comment: item.comment,
            material: item.material,
            multilayer: item.multilayer,
            selectedMaterials: item.selectedMaterials,
            kolvo: tryConvertToNumber(item.kolvo),
            diameter: tryConvertToNumber(item.diameter),
            glubina: tryConvertToNumber(item.glubina),
            minSum: tryConvertToNumber(item.minSum || 0),
            vars: getUpgradedVars(item.vars, item),
            sumItem: tryConvertToNumber(item.sumItem),
            sumItogo: tryConvertToNumber(item.sumItogo),
            sum: tryConvertToNumber(item.sum),
            priceStyle: item.priceStyle
        };
    }

    function upgradeItemType3(item) {
        return {
            work: item.work,
            plos: item.plos,
            comment: item.comment,
            material: item.material,
            glubina: tryConvertToNumber(item.glubina),
            kolvo: tryConvertToNumber(item.kolvo),
            edizm_3: tryConvertToNumber(item.edizm_3),
            edizmStr_3: item.edizmStr_3,
            addSectionName: item.addSectionName,
            addProductName: item.addProductName,
            sumItem: tryConvertToNumber(item.sumItem),
            sumItogo: tryConvertToNumber(item.sumItogo),
            sum: tryConvertToNumber(item.sum),
            priceStyle: item.priceStyle
        };
    }

    function upgradeItemType4(item) {
        return {
            work: item.work,
            plos: item.plos,
            comment: item.comment,
            kolvo: tryConvertToNumber(item.kolvo),
            regionName: item.regionName,
            placeName: item.placeName,
            placeDist: tryConvertToNumber(item.placeDist),
            sumItem: tryConvertToNumber(item.sumItem),
            sumItogo: tryConvertToNumber(item.sumItogo),
            sum: tryConvertToNumber(item.sum),
            priceStyle: item.priceStyle
        };
    }

    function getUpgradedVars(vars, item) {
        return {
            plusSum: +vars.plusSum,
            minusSum: +vars.minusSum,
            plusStr: getSortedFactors(vars.plusStr),
            minusStr: getSortedFactors(vars.minusStr),
            checkbox: vars.checkbox
        };
    }

    function getSortedFactors(str) {
        return str
            .split(', ')
            .sort(factorsComparator)
            .join(', ');

    }

    function factorsComparator(valL, valR) {
        var numL = parseFloat(valL.slice(1).replace('/', '.'));
        var numR = parseFloat(valR.slice(1).replace('/', '.'));

        return numL - numR;
    }

    function tryConvertToNumber(value) {
        var parsedValue = parseFloat(value);

        if (parsedValue || parsedValue === 0) {
            return parsedValue;
        }
    }
}
