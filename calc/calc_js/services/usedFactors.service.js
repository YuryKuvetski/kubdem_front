angular.module('calc')
    .factory('usedFactorsService', usedFactorsService);

usedFactorsService.$inject = ['currentPricesService', '$rootScope', 'commonService'];

function usedFactorsService(currentPrices, $rootScope, common) {
    var service = {
        factors: [],
        updateUsedFactors: updateUsedFactors,
        clear: clear
    };

    return service;

    function updateUsedFactors() {
        var usedFactorsObject = {};
        var sourceFactors = currentPrices.options.factors;
        var sections = $rootScope.sections;
        var discountFactor = {
            code: 6.4,
            description: 'Понижающий коэффициент (индивидуальный)',
            key: 'k_6_4',
            name: '6/4',
            val: []
        };

        service.factors = [];
        sections && sections.forEach(function (section) {
            section.items && section.items.forEach(function (item) {
                if (item.selectedMaterials) {
                    common.MATERIALS_KEYS.forEach(function (key) {
                        var selectedMaterial = item.selectedMaterials[key];

                        if (selectedMaterial && selectedMaterial.discount) {
                            var discount = -selectedMaterial.discount;

                            if (discountFactor.val.every(function (val) { return val !== discount})) {
                                discountFactor.val.push(discount);
                            }
                        }
                    });
                }
                if (item.vars && item.vars.checkbox) {
                    var checkbox = item.vars.checkbox;

                    for (var key in checkbox) {
                        if (checkbox.hasOwnProperty(key) && checkbox[key]) {
                            usedFactorsObject[key] = getFactorByKey(sourceFactors, key);
                        }
                    }
                }
            })
        });

        for (var key in usedFactorsObject) {
            if (usedFactorsObject.hasOwnProperty(key)) {
                var temp = getFactorByKey(sourceFactors, key);
                temp && service.factors.push(temp);
            }
        }

        if (discountFactor.val.length) {
            discountFactor.val = discountFactor.val.join(', ');
            service.factors.push(discountFactor);
        }

        service.factors = service.factors.sort(factorsComparator);
    }

    function getFactorByKey(factors, key) {
        for (var i = 0; i < factors.length; i++) {
            if (factors[i].key === key) {
                return factors[i];
            }
        }
    }

    function factorsComparator(factorL, factorR) {
        return factorL.code - factorR.code;
    }

    function clear() {
        service.factors = [];
    }
}
