angular.module('calc')
    .factory('commonService', commonService);

commonService.$inject = ['$rootScope', 'currentPricesService'];

function commonService($rootScope, currentPrices) {
    var TRANSPORT_WORK_NUMBER = 4;
    var MATERIALS_KEYS = [
        'sideA19', 'sideA18', 'sideA17', 'sideA16', 'sideA15', 'sideA14', 'sideA13', 'sideA12', 'sideA11', 'sideA10',
        'sideA9', 'sideA8', 'sideA7', 'sideA6', 'sideA5', 'sideA4', 'sideA3', 'sideA2', 'sideA1', 'sideA',
        'construct',
        'sideB', 'sideB1', 'sideB2', 'sideB3', 'sideB4', 'sideB5', 'sideB6', 'sideB7', 'sideB8', 'sideB9', 'sideB10',
        'sideB11', 'sideB12', 'sideB13', 'sideB14', 'sideB15', 'sideB16', 'sideB17', 'sideB18', 'sideB19'
    ];
    var MATERIALS_LABELS = [
        'A19', 'A18', 'A17', 'A16', 'A15', 'A14', 'A13', 'A12', 'A11', 'A10', 'A9', 'A8', 'A7', 'A6', 'A5', 'A4', 'A3', 'A2', 'A1', 'A',
        'Конструкционный',
        'B', 'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12', 'B13', 'B14', 'B15', 'B16', 'B17', 'B18', 'B19'];
    var ALL_FACTURES = [
        'air',
        'solid',
        'metal',
        'metal2',
        'nemetall',
        'wood',
        'stone',
        'ceramic',
        'block',
        'beton',
        'beton_arm',
        'beton_napr',
        'glass',
        'eights',
        'vertical',
        'zasypka',
        'grunt',
        'grunt_nasypnoi',
        'pesok',
        'hydroisolation',
        'penoplast',
        'gyps',
        'gypsokarton',
        'glina',
        'asfaltobeton',
        'asfaltobeton2',
        'gravel',
        'scheben',
        'metprofil',
        'plitka',
    ];
    var FACTURES_LABELS = {
        'air': 'Воздух',
        'solid': 'Залито',
        'metal': 'Металлы и твердые сплавы',
        'metal2': 'Металлы и твердые сплавы (1.5x)',
        'nemetall': 'Теплоизоляция',
        'wood': 'Дерево',
        'stone': 'Камень',
        'ceramic': 'Кирпич',
        'block': 'Блок',
        'beton': 'Бетон',
        'beton_arm': 'Армированный бетон',
        'beton_napr': 'Напряженный бетон',
        'glass': 'Стекло',
        'eights': 'Демпферный слой',
        'vertical': 'Вертикальная штриховка',
        'zasypka': 'Обратная засыпка, строительный мусор',
        'grunt': 'Грунт естественный',
        'grunt_nasypnoi': 'Грунт насыпной',
        'pesok': 'Песок, стяжка, штукатурка',
        'hydroisolation': 'Гидроизоляция',
        'penoplast': 'Пенопласт',
        'gyps': 'Гипсовые изделия',
        'gypsokarton': 'Гипсокартон',
        'glina': 'Глина',
        'asfaltobeton': 'Асфальтобетон',
        'asfaltobeton2': 'Асфальтобетон (1.5x)',
        'gravel': 'Гравий',
        'scheben': 'Щебень',
        'metprofil': 'Профиль металлический',
        'plitka': 'Плитка',
    };
    var STROKED_FACTURES = ['plitka'];
    var ALWAYS_SHOWED_BUTTONS_FOR_MATERIAL_SELECTING = {
        sideA: true,
        construct: true,
        sideB: true
    };
    var service = {
        MATERIALS_KEYS: MATERIALS_KEYS,
        MATERIALS_LABELS: MATERIALS_LABELS,
        ALL_FACTURES: ALL_FACTURES,
        FACTURES_LABELS: FACTURES_LABELS,
        ALWAYS_SHOWED_BUTTONS_FOR_MATERIAL_SELECTING: ALWAYS_SHOWED_BUTTONS_FOR_MATERIAL_SELECTING,
        itogo: itogo,
        isEditSections: isEditSections,
        roundToPrecision: roundToPrecision,
        roundToPrecisionStr: roundToPrecisionStr,
        roundToPrecisionStrNoZero: roundToPrecisionStrNoZero,
        printWork: printWork,
        printWorkForItem: printWorkForItem,
        printMaterial: printMaterial,
        printEdizmStr: printEdizmStr,
        printEdizm: printEdizm,
        getById: getById,
        clearRootScope: clearRootScope,
        calcNf: calcNf,
        downloadExcel: downloadExcel,
        getNumSelectedMaterials: getNumSelectedMaterials,
        getMaterialIndex: getMaterialIndex,
        parseInputPercentsFromString: parseInputPercentsFromString,
        clamp: clamp,
        swapInTheArray: swapInTheArray,
        getPrevLayerKey: getPrevLayerKey,
        isStrokedHatch: isStrokedHatch
    };

    return service;

    function itogo() {
        var itogoWork = 0;
        var itogoTransport = 0;

        for (var i = 0; i < $rootScope.sections.length; i++) {
            var section = $rootScope.sections[i];

            section['itogo'] = 0;

            for (var j = 0; j < section.items.length; j++) {
                var sumItem = Math.max((+section.items[j]['sum']) || 0, (+section.items[j]['minSum']) || 0);

                section['itogo'] += sumItem;

                if (section.items[j]['work'] === TRANSPORT_WORK_NUMBER) {
                    itogoTransport += sumItem;
                } else {
                    itogoWork += sumItem;
                }
            }
        }

        $rootScope.sections.discount = itogoWork && itogoWork < currentPrices.minPrice
            ? (currentPrices.minPrice - itogoWork)
            : 0;
        $rootScope.sections.itogo = itogoWork + itogoTransport;
        $rootScope.sections.clientDiscount = $rootScope.sections.isClientDiscount && $rootScope.sections.clientDiscount || 0;
        $rootScope.sections.clientDiscountValue = 0;

        if ($rootScope.sections.isClientDiscount) {
            var discountFactor = 1 - $rootScope.sections.clientDiscount / 100;
            var sumWithoutTransport = itogoWork + $rootScope.sections.discount;

            $rootScope.sections.clientDiscountValue = sumWithoutTransport * (1 - discountFactor);
            $rootScope.sections.sumWithClientDiscount = sumWithoutTransport * discountFactor + itogoTransport;
        }
    }

    function isEditSections() {
        if (!$rootScope.cloneSections) {
            $rootScope.cloneSections = [];
        }

        $rootScope.isPrint = !angular.equals($rootScope.cloneSections, $rootScope.sections);

        return $rootScope.isPrint;
    }

    function roundToPrecision(value, precision) {
        return +roundToPrecisionStr(value, precision);
    }

    function roundToPrecisionStrNoZero(value, precision) {
        return value || value === 0
            ? +roundToPrecisionStr(value, precision)
            : '';
    }

    function roundToPrecisionStr(value, precision) {
        if (!precision && precision !== 0) {
            return (+value).toFixed(currentPrices.MAX_PRECISION);
        }

        return (+value).toFixed(precision);
    }

    function roundForItem(value, workId) {
        return roundToPrecisionStr(value, currentPrices.getPrecisionByWorkId(workId));
    }

    // Вывод наименования на экран
    function printWork(sectionKey, itemKey) {
        return printWorkForItem($rootScope.sections[sectionKey].items[itemKey])
    }

    // Вывод наименования на экран
    function printWorkForItem(item) {
        var workType = getWorkTypeForItem(item);

        // Если сверлим
        if (workType.id === 1) {
            return workType.name + ' Ø' + item.diameterName + '. ';
        }

        // Если режем
        if (workType.id === 2) {
            return getById(currentPrices.options.aparatRezki, item.work_2).name + '. ';
        }

        // Если транспортные расходы
        if (workType.id === 4) {
            return workType.name + ' (' + item.regionName + ', ' + item.placeName + '). ';
        }

        return '';
    }

    // Вывод наименования на экран
    function printMaterial(sectionKey, itemKey) {
        var item = $rootScope.sections[sectionKey].items[itemKey];
        var workType = getWorkTypeForItem(item);

        // Если сверлим
        if (workType.id === 1) {
            return getById(currentPrices.options.materials, item.material).name;
        }

        // Если режем
        if (workType.id === 2) {
            return getById(currentPrices.options.materialRezki, item.material).name;
        }

        // Если дополнительная услуга
        if (workType.id === 3) {
            return item.material;
        }

        return '';
    }

    // Вывод единицы измерения на экран
    function printEdizmStr(sectionKey, itemKey) {
        var item = $rootScope.sections[sectionKey].items[itemKey];
        var workType = getWorkTypeForItem(item);

        // Если дополнительная услуга
        if (workType.id === 3) {
            return item['edizmStr_3'];
        }

        return workType.edizm;
    }

    // Вывод суммы единицы измерения на экран
    function printEdizm(sectionKey, itemKey) {
        var item = $rootScope.sections[sectionKey].items[itemKey];
        var workType = getWorkTypeForItem(item);

        // Если сверлим
        if (workType.id === 1) {
            return roundToPrecision(item['glubina'] * item['kolvo']);
        }
        // Если режем
        if (workType.id === 2) {
            return roundForItem(item['diameter'] * item['glubina'] / 10000, 2);
        }
        // Если дополнительная услуга
        if (workType.id === 3) {
            return item['edizm_3'];
        }
        // Если транспортные расходы
        if (workType.id === 4) {
            return item['placeDist'];
        }

        return '';
    }

    function getWorkTypeForItem(item) {
        return getById(currentPrices.options.work_types, item['work']);
    }

    // Поиск в массиве по id. Возвращаем элемент
    function getById(array, id) {
        for (var i = 0; i < array.length; i++) {
            if (array[i]['id'] === id) {
                return array[i];
            }
        }
    }

    function clearRootScope() {
        $rootScope.sections = [];
        $rootScope.sections.discount = 0;
        $rootScope.sections.clientDiscount = 0;
        $rootScope.sections.isClientDiscount = false;
        $rootScope.sections.itogo = 0;
        $rootScope.curentSection = false;
        $rootScope.nds = false;
    }

    function calcNf(proportions) {
        return Math.round(((proportions.length || 0) * (proportions.width || 0) * (proportions.height || 0) / 1950000) * 10) / 10;
    }

    function downloadExcel(selector, filename) {
        var URL = window.URL || window.webkitURL || window;
        var html = $('<div>').append($(selector).clone()).html();
        var excel = getExcel(html);
        var blob = new Blob([excel], {type: 'application/vnd.ms-excel'});

        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, filename);

            return;
        }

        var excelUrl = URL.createObjectURL(blob);
        var downloadLink = document.createElement('a');

        downloadLink.href = excelUrl;
        downloadLink.download = filename;
        downloadLink.style.display = 'none';

        document.body.appendChild(downloadLink);
        downloadLink.click();

        setTimeout(function () {
            if (downloadLink) {
                downloadLink.remove();
            }

            URL.revokeObjectURL(excelUrl);
        }.bind(this), 100);

        function getExcel(html) {
            var excelFile = '<html xmlns:o=\'urn:schemas-microsoft-com:office:office\' xmlns:x=\'urn:schemas-microsoft-com:office:excel\' xmlns=\'http://www.w3.org/TR/REC-html40\'>';
            excelFile += '<head>';
            excelFile += '<style>body{font-family:"Helvetica Neue","Helvetica","Arial","sans-serif";}</style>';
            excelFile += '<meta http-equiv="Content-type" content="text/html;charset=utf-8" />';
            excelFile += '<!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>worksheet</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->';
            excelFile += '</head>';
            excelFile += '<body>';
            excelFile += html.replace(/"/g, '\'');
            excelFile += '</body>';
            excelFile += '</html>';

            return excelFile;
        }
    }

    function getNumSelectedMaterials(materials) {
        if (!materials) {
            return 0;
        }

        return MATERIALS_KEYS.reduce(function (result, key) {
            return result + (materials[key] ? 1 : 0);
        }, 0);
    }

    function getMaterialIndex(selectedMaterials, materialKey) {
        var result = 1;

        for (var i = 0; i < MATERIALS_KEYS.length; i++) {
            var key = MATERIALS_KEYS[i];

            if (key === materialKey) {
                return result;
            } else if (selectedMaterials[key]) {
                result++;
            }
        }

        return result;
    }

    function parseInputPercentsFromString(value) {
        value = value.toString().replace(',', '.').replace(/[^0-9.]/g, '');

        var parts = value.toString().split('.');
        var left = (+parts[0]) || 0;
        var parsedRight = parseFloat('1.1' + parts[1] + '1').toString().slice(3, -1);
        var right = parts[1] || parts[1] === ''
            ? '.' + (parsedRight[0] || '') + (parsedRight[1] || '')
            : '';

        return left + right;
    }

    function clamp(a, b, c) {
        return Math.max(b, Math.min(c, a));
    }

    function swapInTheArray(array, firstIndex, secondIndex) {
        var tmp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = tmp;
    }

    function getPrevLayerKey(key) {
        var prevCount = parseInt(key.slice(5)) - 1;

        return key.slice(0, 5) + (prevCount ? prevCount.toString() : '');
    }

    function isStrokedHatch(facture) {
        return STROKED_FACTURES.indexOf(facture) !== -1;
    }
}
