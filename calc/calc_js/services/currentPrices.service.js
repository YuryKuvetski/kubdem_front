angular.module('calc')
    .factory('currentPricesService', currentPricesService);

currentPricesService.$inject = ['$http', '$q', '$window'];

function currentPricesService($http, $q, $window) {
    var MATERIAL_KOEFS = [1, 0.8, 1, 1.2, 0.8];
    var FACTORS_OLD = {
        k_1: {
            description: 'Повышенное содержание арматуры (более 150 кг/м3), толщина арматурного стержня 20 мм и более',
            code: 1
        },
        k_2_1: {
            description: 'Тяжелый, плотный бетон класса В25',
            code: 2.1
        },
        k_2_2: {
            description: 'Тяжелый, плотный бетон класса В30',
            code: 2.2
        },
        k_2_3: {
            description: 'Тяжелый, плотный бетон класса В35',
            code: 2.3
        },
        k_3: {
            description: 'Работа без возможности подвода воды',
            code: 3
        },
        k_4: {
            description: 'Работа в труднодоступных, опасных местах  или в стесненных условиях',
            code: 4
        },
        k_5_1: {
            description: 'Работа на высоте от 1,80 до 3,00 м',
            code: 5.1
        },
        k_5_2: {
            description: 'Работа на высоте от 3,01 до 4,50 м',
            code: 5.2
        },
        k_5_3: {
            description: 'Работа на высоте от 4,51 до 6,50 м',
            code: 5.3
        },
        k_6: {
            description: 'Резка проемов методом алмазного бурения (строчное сверление)',
            code: 6
        },
        k_7_1: {
            description: 'Статус постоянного клиента - стандарт',
            code: 7.1
        },
        k_7_2: {
            description: 'Статус постоянного клиента - профи',
            code: 17.2
        },
        k_7_3: {
            description: 'Статус постоянного клиента - эксперт',
            code: 7.3
        },
        k_8_1: {
            description: 'Сверление от 50 до 100 отверстий',
            code: 8.1
        },
        k_8_2: {
            description: 'Сверление от 101 до 200 отверстий',
            code: 8.2
        },
        k_8_3: {
            description: 'Сверление от 201 отверстия',
            code: 8.3
        },
        k_9: {
            description: 'Необходимость проведения работ с 18.00 до 8.00 (ночное время суток)',
            code: 9
        },
        k_10: {
            description: 'Необходимость проведения работ в выходные и праздничные дни',
            code: 10
        },
        k_11: {
            description: 'Отсутствие электричества, работа с бензогенератором (до 6.5 кВт⋅ч), без учета стоимости топлива',
            code: 11
        },
        k_12: {
            description: 'Глубина сверления при последующем увеличении глубины сверления на шаг равный 30 см, от базовой 43 см',
            code: 12
        },
        k_13: {
            description: 'Работы с потолком (снизу вверх)',
            code: 13
        },
        k_14: {
            description: 'Проведение работ под углом к обрабатываемой поверхности',
            code: 14
        },
        k_15: {
            description: 'Проведение работ при отсутствии возможности механического крепления оборудования анкером',
            code: 15
        },
        k_16_1: {
            description: 'Сверление без станины (вручную до Ø 252 мм) - блок, кирпич',
            code: 16.1
        },
        k_16_2: {
            description: 'Сверление без станины (вручную до Ø 252 мм) - камень строительный, бетон, армированный бетон',
            code: 16.2
        },
        k_17: {
            description: 'Сверление на «сухую» - блок, кирпич',
            code: 17
        },
        k_18: {
            description: 'Сбор воды коллектором при помощи пылесоса',
            code: 18
        },
        k_19: {
            description: 'Эксклюзивный демонтаж (идеальная чистота)',
            code: 19
        },
        k_20: {
            description: 'Сверление с улавливанием керна (при вертикальном сверлении)',
            code: 20
        },
        k_23: {
            description: 'Проведение работ по отвесу',
            code: 23
        },
        k_24: {
            description: 'Уборка кернов диаметром до 250 мм (складирование на территории объекта)',
            code: 24
        }
    };
    var DEFAULT_MAX_PRECISION = 2;
    var DEFAULT_WORK_TYPES = [
        {id: 1, name: 'Алмазное сверление', edizm: 'см. пог.', precision: 1},
        {id: 2, name: 'Алмазная резка', edizm: 'м. кв.', precision: 2},
        {id: 3, name: 'Дополнительная услуга', edizm: 'шт.', precision: 0},
        {id: 4, name: 'Транспортные расходы', edizm: 'км.', precision: 0}
    ];
    var DEFAULT_MATERIAL_REZKI = [
        {id: 1, name: 'Асфальт'},
        {id: 2, name: 'Блок, кирпич'},
        {id: 3, name: 'Бетон, камень'},
        {id: 4, name: 'Армированный бетон'}
    ];
    var DEFAULT_APARAT_REZKI = [
        {
            id: 1,
            name: 'Штроборез с пылесосом',
            1: 0,
            2: 3750,
            3: 5000,
            4: 6250,
            tip: '220 В',
            pros: 'только сухой',
            max: 120
        },
        {
            id: 2,
            name: 'Бензорез',
            1: 3900,
            2: 4875,
            3: 6500,
            4: 8125,
            tip: 'Бензин',
            pros: 'сухой возможен / мокрый',
            max: 150
        },
        {
            id: 3,
            name: 'Cut-n-breake',
            1: 4800,
            2: 6000,
            3: 8000,
            4: 10000,
            tip: '220 В',
            pros: 'сухой возможен / мокрый',
            max: 400
        },
        {
            id: 4,
            name: 'Швонерезчик',
            1: 3000,
            2: 0,
            3: 5000,
            4: 6250,
            tip: 'Бензин, дизель, 380 В',
            pros: 'только мокрый',
            max: 320
        },
        {
            id: 5,
            name: 'Стенарезная машина',
            1: 4800,
            2: 6000,
            3: 8000,
            4: 10000,
            tip: '380 В',
            pros: 'сухой возможен / мокрый',
            max: 730
        },
        {
            id: 6,
            name: 'Канатная машина',
            1: 7200,
            2: 9000,
            3: 12000,
            4: 15000,
            tip: '220 В / 380 В',
            pros: 'сухой возможен / мокрый',
            max: 4000
        }
    ];
    var DEFAULT_FACTORS_ADDITIONAL_INFO = {
        glubina: {
            key: 12,
            start: 43,
            step: 30
        }
    };
    var DEFAULT_MIN_PRICE = 1500;
    var currentPricesBackup = {};
    var service = {
        MAX_PRECISION: DEFAULT_MAX_PRECISION,
        priceId: null,
        minPrice: DEFAULT_MIN_PRICE,
        isMinPriceEdited: false,
        factorsPerColumn: [],
        options: {
            diameters: [],
            materials: [],
            factors: [],
            factors1: [],
            factors2: [],
            factorsAdditionalInfo: DEFAULT_FACTORS_ADDITIONAL_INFO,
            work_types: DEFAULT_WORK_TYPES,
            materialRezki: [],
            aparatRezki: [],
            regions: [],
            farePrice: null,
            addSections: []
        },
        loadCurrentPrices: loadCurrentPrices,
        loadById: loadById,
        loadFromOptions: loadFromOptions,
        saveFactorsPerColumn: saveFactorsPerColumn,
        getFactorByKey: getFactorByKey,
        getFactorByCode: getFactorByCode,
        getDiameterByName: getDiameterByName,
        getRegionByName: getRegionByName,
        getPlaceByName: getPlaceByName,
        priceDiameter: priceDiameter,
        getAddSectionByName: getAddSectionByName,
        getAddProductByName: getAddProductByName,
        getPrecisionByWorkId: getPrecisionByWorkId,
        clear: clear,
        setFactorsByWorkId: setFactorsByWorkId
    };

    loadCurrentPrices();

    return service;

    function loadById(priceId, item) {
        var promises = [
            getPromiseForPrice(priceId.type_1),
            getPromiseForPrice(priceId.type_2),
            getPromiseForPrice(priceId.type_3),
            getPromiseForPrice(priceId.type_4)
        ];

        return $q.all(promises)
            .then(function (options) {
                service.priceId = priceId;
                service.minPrice = item.minPrice || DEFAULT_MIN_PRICE;
                service.isMinPriceEdited = item.isMinPriceEdited || false;
                updateOptions({
                    type_1: options[0] && options[0].data.price,
                    type_2: options[1] && options[1].data.price,
                    type_3: options[2] && options[2].data.price,
                    type_4: options[3] && options[3].data.price
                });
            });
    }

    function getPromiseForPrice(priceId) {
        if (!priceId) {
            return $q(function (resolve) {
                return resolve();
            });
        }

        // return $http.get('https://kubdem.ru/mods/price/item?id=' + priceId);
        return $http.get('/mock/price/item_id=' + priceId + '.json');
    }

    function loadFromOptions(options, item) {
        service.priceId = null;
        service.options = options;
        service.minPrice = item.minPrice || DEFAULT_MIN_PRICE;
        service.isMinPriceEdited = item.isMinPriceEdited || false;

        upgradeDiameters();
        upgradeFactors();
    }

    function loadCurrentPrices() {
        // return $http.get('https://kubdem.ru/mods/price/current-prices')
        return $http.get('/mock/current-prices.json')
            .then(function (responce) {
                if (responce.data) {
                    currentPricesBackup = responce.data;
                    setCurrentPrices(responce.data);
                }
            });
    }

    function setCurrentPrices(currPrices) {
        service.minPrice = currPrices.minPrice || DEFAULT_MIN_PRICE;
        service.isMinPriceEdited = currPrices.isMinPriceEdited || false;

        service.priceId = {
            type_1: currPrices.type_1 && currPrices.type_1.id,
            type_2: currPrices.type_2 && currPrices.type_2.id,
            type_3: currPrices.type_3 && currPrices.type_3.id,
            type_4: currPrices.type_4 && currPrices.type_4.id
        };
        updateOptions({
            type_1: currPrices.type_1 && currPrices.type_1.data,
            type_2: currPrices.type_2 && currPrices.type_2.data,
            type_3: currPrices.type_3 && currPrices.type_3.data,
            type_4: currPrices.type_4 && currPrices.type_4.data
        });
    }

    function updateOptions(options) {
        setDefaultOptions();

        var dataForType1 = options.type_1;
        var dataForType2 = options.type_2;
        var dataForType3 = options.type_3;
        var dataForType4 = options.type_4;

        if (dataForType1) {
            service.options.work_types[0].precision = +(dataForType1.params && dataForType1.params.round);
            service.options.factorsAdditionalInfo.glubina = dataForType1.params && dataForType1.params.glubina
                || DEFAULT_FACTORS_ADDITIONAL_INFO.glubina;

            dataForType1.items && dataForType1.result && setDiameters(dataForType1.items, dataForType1.result);

            if (dataForType1.groups) {
                service.options.materials = parseMaterials(dataForType1.groups);
            }

            if (dataForType1.factors) {
                service.options.factors1 = dataForType1.factors;
                service.options.factors = parseFactors(dataForType1.factors);
            }
        }

        if (dataForType2) {
            service.options.work_types[0].precision = +(dataForType2.params && dataForType2.params.round);
            service.options.factorsAdditionalInfo.glubina = dataForType2.params && dataForType2.params.glubina
                || DEFAULT_FACTORS_ADDITIONAL_INFO.glubina;

            dataForType2.items && dataForType2.result && setAparatRezki(dataForType2.items, dataForType2.result);

            if (dataForType2.groups) {
                service.options.materialRezki = parseMaterials(dataForType2.groups);
            }

            if (dataForType2.factors) {
                service.options.factors2 = dataForType2.factors;
            }
        }

        if (dataForType3 && dataForType3.result) {
            service.options.work_types[2].precision = +(dataForType3.params && dataForType3.params.round);
            setAdditionalSections(dataForType3.result);
        }

        if (dataForType4 && dataForType4.result) {
            service.options.work_types[3].precision = 0;
            service.options.farePrice = dataForType4.params && dataForType4.params.sum || 0;
            setRegions(dataForType4.result);
        }

        service.MAX_PRECISION = Math.max([
            service.options.work_types[0].precision,
            service.options.work_types[1].precision,
            service.options.work_types[2].precision,
            service.options.work_types[3].precision,
            DEFAULT_MAX_PRECISION
        ]) || DEFAULT_MAX_PRECISION;
    }

    function setDefaultOptions() {
        service.options = {
            diameters: [],
            materials: [],
            factors: [],
            factorsAdditionalInfo: DEFAULT_FACTORS_ADDITIONAL_INFO,
            work_types: DEFAULT_WORK_TYPES,
            materialRezki: DEFAULT_MATERIAL_REZKI,
            aparatRezki: DEFAULT_APARAT_REZKI,
            regions: [],
            farePrice: null,
            addSections: []
        };
    }

    function setDiameters(diameters, diametersPrices) {
        service.options.diameters = diameters.map(function (item) {
            var diameterPrices = [];

            for (var i = 0; i < diametersPrices.length; i++) {
                if (diametersPrices[i].k === item.diameter) {
                    diameterPrices = diametersPrices[i].v;
                    break;
                }
            }

            return {
                name: item.diameter,
                prices: diameterPrices
            };
        });
    }

    function setAparatRezki(aparatsData, pricesData) {
        service.options.aparatRezki = aparatsData.map(function (aparat, index) {
            var prices = [];

            for (var i = 0; i < pricesData.length; i++) {
                if (pricesData[i].k === aparat.instrument) {
                    prices = pricesData[i].v;
                    break;
                }
            }

            return {
                id: index + 1,
                name: aparat.instrument,
                max: aparat.max,
                tip: aparat.tip,
                pros: aparat.pros,
                prices: prices
            };
        });
    }

    function parseMaterials(materials) {
        return materials.map(function (group, id) {
            group.id = id + 1;
            return group;
        });
    }

    function parseFactors(factors) {
        if (!factors) {
            return [];
        }

        var result = factors.map(function (factor) {
            var key = ('k_' + factor.code).replace('.', '_');
            var name = ('' + factor.code).replace('.', '/');

            return {
                key: key,
                name: name,
                code: factor.code,
                val: factor.param,
                description: factor.name
            };
        });

        calcFactorsCount(result);

        return result;
    }

    function setRegions(data) {
        service.options.regions = data.map(function (region) {
            var places = region.items.filter(function (place) {
                return place.name && isFinite(place.km);
            });

            return {
                name: region.name,
                places: places
            }
        });
    }

    function setAdditionalSections(data) {
        var edStrs = {
            1: 'шт',
            2: 'см',
            3: 'кг',
            4: 'ед'
        };

        service.options.addSections = data.map(function (section) {
            var products = section.items.filter(function (product) {
                return product.name && product.ed && isFinite(product.sum);
            })
                .map(function (product) {
                    product.edStr = edStrs[product.ed];

                    return product;
                });

            return {
                name: section.name,
                products: products
            }
        });
    }

    function upgradeDiameters() {
        service.options.diameters = service.options.diameters
            .filter(function (diameter) {
                return isFinite(diameter.name);
            })
            .map(function (diameter) {
                var diameterPrices = [];

                service.options.materials.forEach(function (material) {
                    diameterPrices.push(priceDiameter(diameter.name, material.id));
                });

                return {
                    name: diameter.name,
                    prices: diameterPrices
                };
            });
    }

    function priceDiameter(diameter, material) {
        diameter = Math.max(diameter, 90);
        var korr300 = diameter > 299 ? service.options.korrectirDiameter300 : 1;

        // Формула расчета стоимости сверления
        var out = Math.ceil((diameter + 2) / 1000 * 3.14 * (8000 / 1.2) / 100 * korr300 * service.options.korrectirDiameter);
        out = Math.ceil(out * (MATERIAL_KOEFS[material] || 1));

        return out;
    }

    function upgradeFactors() {
        var varsPrice = service.options.varsPrice;

        service.options.factors = [];
        service.options.factorsAdditionalInfo = DEFAULT_FACTORS_ADDITIONAL_INFO;

        for (var key in FACTORS_OLD) {
            if (varsPrice[key]) {
                service.options.factors.push({
                    key: key,
                    name: varsPrice[key].name,
                    val: varsPrice[key].val,
                    code: FACTORS_OLD[key].code,
                    description: FACTORS_OLD[key].description
                });
            }
        }

        calcFactorsCount();
    }

    function calcFactorsCount(factors) {
        var data = $window.localStorage.getItem('factorsPerColumn');
        var factorsPerColumn = data && angular.fromJson(data);

        if (factorsPerColumn) {
            service.factorsPerColumn = factorsPerColumn;
        } else {
            var length = factors && factors.length
                ? factors.length
                : service.options.factors.length;
            var average = Math.ceil(length / 3);

            service.factorsPerColumn = [average, average, length - (2 * average)];
        }
    }

    function saveFactorsPerColumn(minValue) {
        for (var i = 0; i < 3; i++) {
            if (!service.factorsPerColumn[i] || service.factorsPerColumn[i] < minValue) {
                service.factorsPerColumn[i] = minValue;
            } else if (service.factorsPerColumn[i] > service.options.factors.length) {
                service.factorsPerColumn[i] = service.options.factors.length;
            }
        }
        $window.localStorage.setItem('factorsPerColumn', angular.toJson(service.factorsPerColumn));
    }

    function getFactorByKey(key) {
        return getByKey(service.options.factors, 'key', key);
    }

    function getFactorByCode(code) {
        return getByKey(service.options.factors, 'code', code);
    }

    function getDiameterByName(name) {
        return getByKey(service.options.diameters, 'name', name);
    }

    function getRegionByName(name) {
        return getByKey(service.options.regions, 'name', name);
    }

    function getPlaceByName(places, name) {
        return getByKey(places, 'name', name);
    }

    function getAddSectionByName(name) {
        return getByKey(service.options.addSections, 'name', name);
    }

    function getAddProductByName(products, name) {
        return getByKey(products, 'name', name);
    }

    function getByKey(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
    }

    function getPrecisionByWorkId(workId) {
        var precision = service.options.work_types[workId - 1] && service.options.work_types[workId - 1].precision;

        if (precision || precision === 0) {
            return precision;
        }
    }

    function clear() {
        setCurrentPrices(currentPricesBackup);
    }

    function setFactorsByWorkId(workId) {
        service.options.factors = parseFactors(service.options['factors' + workId]);
    }
}
