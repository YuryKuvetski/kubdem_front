var app = angular.module('price', ['ngRoute', 'ui.bootstrap']);

//<editor-fold defaultstate="collapsed" desc="Route">
app.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.
                when('/', {
                    templateUrl: '/list.html?' + mod_price_ver,
                    controller: 'listCtrl'
                }).
                when('/item1', {
                    templateUrl: '/item1.html?' + mod_price_ver,
                    controller: 'item1Ctrl'
                }).
                when('/item1/:id', {
                    templateUrl: '/item1.html?' + mod_price_ver,
                    controller: 'item1Ctrl'
                }).
                when('/item2', {
                    templateUrl: '/item2.html?' + mod_price_ver,
                    controller: 'item2Ctrl'
                }).
                when('/item2/:id', {
                    templateUrl: '/item2.html?' + mod_price_ver,
                    controller: 'item2Ctrl'
                }).
                when('/item3', {
                    templateUrl: '/item3.html?' + mod_price_ver,
                    controller: 'item3Ctrl'
                }).
                when('/item3/:id', {
                    templateUrl: '/item3.html?' + mod_price_ver,
                    controller: 'item3Ctrl'
                }).
                when('/item4', {
                    templateUrl: '/item4.html?' + mod_price_ver,
                    controller: 'item4Ctrl'
                }).
                when('/item4/:id', {
                    templateUrl: '/item4.html?' + mod_price_ver,
                    controller: 'item4Ctrl'
                }).
                otherwise({
                    redirectTo: '/'
                });
    }]);
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="listCtrl">
app.controller('listCtrl', ['$rootScope', '$scope', '$http', '$location', function ($rootScope, $scope, $http, $location) {

        // Права текущего пользователя
        $rootScope.access = mod_price_access;

        $rootScope.isLoad = true;

        // Загружаем прайс-листы с сервера
        $http.get('/mock/price/load' + '.json')
                .success(function (data) {
                    if (data === false) {
                        alert('Произошла ошибка! Попробуйте еще раз.');
                    }
                    if (data !== false) {
                        $scope.prices = data.prices;
                        $rootScope.branche = data.branche;
                        $rootScope.isLoad = false;
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен!');
                });

        // Переход к прайс-листу
        $scope.viewPrice = function (item) {
            if (item['type'] === '1') {
                window.location.href = '#/item1/' + item['id'];
            }
            if (item['type'] === '4') {
                window.location.href = '#/item2/' + item['id'];
            }
            if (item['type'] === '3') {
                window.location.href = '#/item3/' + item['id'];
            }
            if (item['type'] === '2') {
                window.location.href = '#/item4/' + item['id'];
            }
        };

    }]);
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="item1Ctrl - прайс-лист на алмазное бурение">
app.controller('item1Ctrl', ['$rootScope', '$scope', '$http', '$location', '$routeParams', '$timeout', 'orderByFilter',
    function ($rootScope, $scope, $http, $location, $routeParams, $timeout, orderBy) {
        var DEFAULT_MIN_PRICE = 1500;

        // Права текущего пользователя
        $rootScope.access = mod_price_access;

        //<editor-fold defaultstate="collapsed" desc="Формируем начальный прайс-лист"> 

        $scope.savePriceProcess = false;

        var price = {
            groups: [
                {name: 'Блок, кирпич', sum: 6000, min: 0},
                {name: 'Бетон, камень', sum: 7500, min: 0},
                {name: 'Армированный бетон', sum: 9000, min: 0}
            ],
            items: [
                {diameter: 100, factor: 1, zise: 1},
                {diameter: 120, factor: 1, zise: 1},
                {diameter: 450, factor: 1.2, zise: 1}
            ],
            factors: [
                {code: 1, param: 0.2, name: "Повышенное содержание арматуры (более 150 кг/м3), толщина арматурного стержня 20 мм и более."},
                {code: 7.1, param: -0.05, name: "Скидка постоянному клиенту 5%"},
                {code: 2.1, param: 0.2, name: "Тяжелый, плотный бетон класса: B25"}
            ],
            result: [],
            params: {
                branche: 0,
                type: 1,
                round: 0
            },
            comment: ''
        };

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="functions">

        // Удаляем ценовую группу
        $scope.delGroup = function (key) {
            if ($scope.price.groups.length === 1) {
                return;
            }
            $scope.price.groups.splice(key, 1);
        };

        // Добавляем ценовую группу
        $scope.addGroup = function () {
            var length = $scope.price.groups.length + 1;
            if (length > 5) {
                return false;
            }
            $scope.price.groups.push({
                name: 'Цена ' + length,
                sum: 0
            });
            return false;
        };

        // Удаляем прайсовую позицию
        $scope.delItem = function (key) {
            if ($scope.price.items.length === 1) {
                return;
            }
            $scope.price.items.splice(key, 1);
        };

        // Добавляем прайсовую позицию
        $scope.addItem = function () {
            $scope.price.items.push({
                diameter: 0,
                factor: 1,
                zise: 1
            });
            $scope.price.items = orderBy($scope.price.items, 'diameter', false);
            return false;
        };

        // Удаляем коэффициент
        $scope.delFactor = function (key) {
            if ($scope.price.factors.length === 1) {
                return;
            }
            $scope.price.factors.splice(key, 1);
        };

        // Добавляем коэффициент
        $scope.addFactor = function () {
            $scope.price.factors.push({
                code: 0,
                param: 0,
                name: '???'
            });
            $scope.price.factors = orderBy($scope.price.factors, 'code', false);
            return false;
        };

        // Высчитываем стоимость всех прайсовых позиций
        $scope.sumItems = function () {
            var oldResults = $scope.price.result;
            $scope.price.result = [];
            angular.forEach($scope.price.items, function (v, k) {
                $scope.sumItem(k);
            });
            $scope.price.items = orderBy($scope.price.items, 'diameter', false);
            $scope.price.factors = orderBy($scope.price.factors, 'code', false);
            applyConstValues();

            function applyConstValues() {
                angular.forEach($scope.price.result, function (v, k) {
                    var value = findByKey(oldResults, v['k']);

                    if (value && v['k'] === value['k'] && value['const']) {
                        v['const'] = true;
                        v['constValues'] = value['constValues'];
                    }
                });
            }

            function findByKey(arr, key) {
                return arr.find(function (value) {
                    return value['k'] === key;
                });
            }
        };

        // Узнаем наименование филиала по номеру
        $scope.brancheName = '';
        $scope.fBrancheName = function () {
            angular.forEach($rootScope.branche, function (v, k) {
                if (v.id === $scope.price.params.branche) {
                    $scope.brancheName = '(' + v.name + ')';
                }
            });
        };

        // Высчитываем стоимость прайсовой позиции
        $scope.sumItem = function (key) {
            var G2 = $scope.price.params.round;
            var L1 = $scope.price.items[key].diameter;
            var L2 = $scope.price.items[key].factor;
            var L3 = $scope.price.items[key].zise;
            var PI = 3.14159;
            var Result = [];
            angular.forEach($scope.price.groups, function (v, k) {
                var G1 = v['sum'];
                var out = Math.ceil((L1 + L3 * 2) / 1000 * PI * G1 / 100 * L2 * 100) / 100;
                if (G2 == 1) {
                    out = Math.ceil(out * 10) / 10;
                }
                if (G2 == 0) {
                    out = Math.ceil(out);
                }
                if (out < v['min']) out = v['min'];
                Result[k] = out;
            });
            $scope.price.result.push({'k': L1, 'v': Result});
        };

        // Сохранение прайс-листа на сервере
        $scope.savePrice = function () {
            $scope.savePriceProcess = true;
            var priceToSave = applyConstValues();

            $http.post('/mods/price/item', {priceId: $scope.priceId, price: priceToSave})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка сохранения! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });

            function applyConstValues() {
                var result = angular.copy($scope.price);

                angular.forEach(result.result, function (v, k) {
                    if (v['const']) {
                        v['v'] = v['constValues'];
                    } else {
                        delete  v['const'];
                        delete  v['constValues'];
                    }
                });

                return result;
            }
        };

        // Прайс был изменен?
        $scope.isEdit = function () {
            if ($scope.price === false) {
                return false;
            }
            if (
                    angular.equals($scope.price.groups, $scope.priceOriginal.groups) &&
                    angular.equals($scope.price.items, $scope.priceOriginal.items) &&
                    angular.equals($scope.price.factors, $scope.priceOriginal.factors) &&
                    angular.equals($scope.price.comment, $scope.priceOriginal.comment) &&
                    angular.equals($scope.price.params, $scope.priceOriginal.params) &&
                        isResultChanged()
                    ) {
                return false;
            } else {
                return true;
            }

            function isResultChanged() {
                var result = true;

                angular.forEach($scope.price.result, function (v, k) {
                    if (!result) {
                        return;
                    }

                    var val1 = v['const']
                        ? v['constValues']
                        : v['v'];
                    var val2 = $scope.priceOriginal.result[k]['const']
                        ? $scope.priceOriginal.result[k]['constValues']
                        : $scope.priceOriginal.result[k]['v'];

                    result = angular.equals(val1, val2);
                });

                return result;
            }
        };

        // Получаем прайс-лист с сервера
        $scope.loadPrice = function (id) {
            $scope.priceIsLoad = true;
            $http.get('/mock/price/item_id=' + id + '.json')
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                        }
                        if (data !== false) {
                            $scope.priceIsLoad = false;
                            $scope.price = data.price;
                            if ($scope.price.result) {
                                $scope.price.params.minPrice = $scope.price.params.minPrice || DEFAULT_MIN_PRICE;
                            }
                            $scope.priceOriginal = angular.copy(data.price);
                            $scope.priceId = data.id;
                            $scope.editBan = data.editBan;
                            $scope.beginning = data.beginning;
                            $rootScope.branche = data.branche;
                            $scope.fBrancheName();
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                    });
        };

        // Удаляем прайс-лист с сервера
        $scope.removePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/remove', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка удаления! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Клонируем прайс-лист
        $scope.clonePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/clone', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка клонирования! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Подписываем прайс-лист
        $scope.signaturePrice = function () {

            if (!confirm("Редактировать и удалить подписанный\nпрайс-лист невозможно. Продолжить?")) {
                return false;
            }

            $scope.savePriceProcess = true;
            $http.post('/mods/price/signature', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Запускаем прайс-лист в работу
        $scope.beginningPrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/beginning', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // включаем\отключаем жестко забитую стоимость
        $scope.toggleConstCosts = function (value) {
            value['const'] = !value['const'];

            if (!value['constValues']) {
                value['constValues'] = angular.copy(value['v']);
            }

            if (value['const']) {
                angular.forEach($scope.price.groups, function (v, k) {
                    value['constValues'][k] = value['constValues'][k] || value['constValues'][k] === 0
                        ? value['constValues'][k]
                        : value['v'][k];
                });
            }
        };

        $scope.constValuesChanges = function (constValues, key) {
            var newValue = constValues[key] < 0
                ? Math.abs(constValues[key])
                : constValues[key] || 0;

            $timeout(function () {
                constValues[key] = newValue;
            });
        };
        //</editor-fold>

        if ($routeParams.id === undefined) {
            $scope.price = price;
            $scope.priceOriginal = angular.copy(price);
            $scope.priceId = 0;
            $scope.priceIsLoad = false;
            $scope.priceNomer = '';

            if ($rootScope.branche === undefined) {
                $location.path('#/');
            }

        } else {
            $scope.price = false;
            $scope.loadPrice($routeParams.id);
            $scope.priceIsLoad = true;
            $scope.priceNomer = '№' + $routeParams.id;
        }

    }]);
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="item2Ctrl - прайс-лист на транспортные расходы">
app.controller('item2Ctrl', ['$rootScope', '$scope', '$http', '$location', '$routeParams', 'orderByFilter',
    function ($rootScope, $scope, $http, $location, $routeParams, orderBy) {

        // Права текущего пользователя
        $rootScope.access = mod_price_access;

        //<editor-fold defaultstate="collapsed" desc="Формируем начальный прайс-лист"> 

        $scope.savePriceProcess = false;

        var price = {
            result: [
                {name: 'Анапский район', items: [{name: 'Витязево', km: 15}]}
            ],
            params: {
                branche: 0,
                type: 4,
                sum: 20
            }
        };

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="functions">

        // Удаляем район
        $scope.delGroup = function (key) {
            if (!confirm("Удалить район и все населенные пункты в нем?")) {
                return false;
            }
            if ($scope.price.result.length === 1) {
                return;
            }
            $scope.price.result.splice(key, 1);
        };

        // Добавляем район
        $scope.addGroup = function () {
            var length = $scope.price.result.length + 1;
            if (length > 9) {
                return false;
            }
            $scope.price.result.push({
                name: null,
                items: [{name: null, km: null}]
            });
            $scope.price.result = orderBy($scope.price.result, 'name', false);
            return false;
        };

        // Удаляем прайсовую позицию
        $scope.delItem = function (group, key) {
            if ($scope.price.result[group].items.length === 1) {
                return;
            }
            $scope.price.result[group].items.splice(key, 1);
        };

        // Добавляем прайсовую позицию
        $scope.addItem = function (group) {
            $scope.price.result[group].items.push({
                name: null,
                km: null
            });
            $scope.price.result[group].items = orderBy($scope.price.result[group].items, 'name', false);
            return false;
        };

        // Сортировка прайс листа
        $scope.view = function () {
            $scope.price.result = orderBy($scope.price.result, 'name', false);
            angular.forEach($scope.price.result, function (v, k) {
                $scope.price.result[k].items = orderBy($scope.price.result[k].items, 'name', false);
            });
        };

        $scope.itog = function (km) {
            return Math.ceil(km * $scope.price.params.sum / 10) * 10;
        };

        // Узнаем наименование филиала по номеру
        $scope.brancheName = '';
        $scope.fBrancheName = function () {
            angular.forEach($rootScope.branche, function (v, k) {
                if (v.id === $scope.price.params.branche) {
                    $scope.brancheName = '(' + v.name + ')';
                }
            });
        };

        // Сохранение прайс-листа на сервере
        $scope.savePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/item', {priceId: $scope.priceId, price: $scope.price})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка сохранения! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Прайс был изменен?
        $scope.isEdit = function () {
            if ($scope.price === false) {
                return false;
            }
            if (
                    angular.equals($scope.price.result, $scope.priceOriginal.result) &&
                    angular.equals($scope.price.params, $scope.priceOriginal.params)
                    ) {
                return false;
            } else {
                return true;
            }

        };

        // Получаем прайс-лист с сервера
        $scope.loadPrice = function (id) {
            $scope.priceIsLoad = true;
            $http.get('/mock/price/item_id=' + id + '.json')
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                        }
                        if (data !== false) {
                            $scope.priceIsLoad = false;
                            $scope.price = data.price;
                            $scope.priceOriginal = angular.copy(data.price);
                            $scope.priceId = data.id;
                            $scope.editBan = data.editBan;
                            $scope.beginning = data.beginning;
                            $rootScope.branche = data.branche;
                            $scope.fBrancheName();
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                    });
        };

        // Удаляем прайс-лист с сервера
        $scope.removePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/remove', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка удаления! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Клонируем прайс-лист
        $scope.clonePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/clone', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка клонирования! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Подписываем прайс-лист
        $scope.signaturePrice = function () {

            if (!confirm("Редактировать и удалить подписанный\nпрайс-лист невозможно. Продолжить?")) {
                return false;
            }

            $scope.savePriceProcess = true;
            $http.post('/mods/price/signature', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Запускаем прайс-лист в работу
        $scope.beginningPrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/beginning', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        //</editor-fold>

        if ($routeParams.id === undefined) {
            $scope.price = price;
            $scope.priceOriginal = angular.copy(price);
            $scope.priceId = 0;
            $scope.priceIsLoad = false;
            $scope.priceNomer = '';

            if ($rootScope.branche === undefined) {
                $location.path('#/');
            }

        } else {
            $scope.price = false;
            $scope.loadPrice($routeParams.id);
            $scope.priceIsLoad = true;
            $scope.priceNomer = '№' + $routeParams.id;
        }

    }]);
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="item3Ctrl - универсальный прайс-лист">
app.controller('item3Ctrl', ['$rootScope', '$scope', '$http', '$location', '$routeParams', 'orderByFilter',
    function ($rootScope, $scope, $http, $location, $routeParams, orderBy) {

        // Права текущего пользователя
        $rootScope.access = mod_price_access;

        //<editor-fold defaultstate="collapsed" desc="Формируем начальный прайс-лист"> 

        $scope.savePriceProcess = false;

        var price = {
            result: [
                {name: 'Раздел №1', items: [{name: null,ed: '1',sum: null}]},
                {name: 'Раздел №2', items: [{name: null,ed: '1',sum: null}]}
            ],
            params: {
                branche: 0,
                type: 3,
                round: 0
            }
        };

        $scope.ed = {'1':'шт','2':'см','3':'кг','4':'ед'};

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="functions">

        // Удаляем раздел
        $scope.delGroup = function (key) {
            if (!confirm("Удалить раздел и все позиции в нем?")) {
                return false;
            }
            if ($scope.price.result.length === 1) {
                return;
            }
            $scope.price.result.splice(key, 1);
        };

        // Добавляем раздел
        $scope.addGroup = function () {
            var length = $scope.price.result.length + 1;
            if (length > 9) {
                return false;
            }
            $scope.price.result.push({
                name: 'Раздел №'+length,
                items: [{name: null,ed: '1',sum: null}]
            });
            $scope.price.result = orderBy($scope.price.result, 'name', false);
            return false;
        };

        // Удаляем прайсовую позицию
        $scope.delItem = function (group, key) {
            if ($scope.price.result[group].items.length === 1) {
                return;
            }
            $scope.price.result[group].items.splice(key, 1);
        };

        // Добавляем прайсовую позицию
        $scope.addItem = function (group) {
            $scope.price.result[group].items.push({
                name: null,
                ed: '1',
                sum: null
            });
            $scope.price.result[group].items = orderBy($scope.price.result[group].items, 'name', false);
            return false;
        };

        // Сортировка прайс листа
        $scope.view = function () {
            $scope.price.result = orderBy($scope.price.result, 'name', false);
            angular.forEach($scope.price.result, function (v, k) {
                $scope.price.result[k].items = orderBy($scope.price.result[k].items, 'name', false);
            });
        };

        // Узнаем наименование филиала по номеру
        $scope.brancheName = '';
        $scope.fBrancheName = function () {
            angular.forEach($rootScope.branche, function (v, k) {
                if (v.id === $scope.price.params.branche) {
                    $scope.brancheName = '(' + v.name + ')';
                }
            });
        };

        // Сохранение прайс-листа на сервере
        $scope.savePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/item', {priceId: $scope.priceId, price: $scope.price})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка сохранения! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Прайс был изменен?
        $scope.isEdit = function () {
            if ($scope.price === false) {
                return false;
            }
            if (
                    angular.equals($scope.price.result, $scope.priceOriginal.result) &&
                    angular.equals($scope.price.params, $scope.priceOriginal.params)
                    ) {
                return false;
            } else {
                return true;
            }

        };

        // Получаем прайс-лист с сервера
        $scope.loadPrice = function (id) {
            $scope.priceIsLoad = true;
            $http.get('/mock/price/item_id=' + id + '.json')
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                        }
                        if (data !== false) {
                            $scope.priceIsLoad = false;
                            $scope.price = data.price;
                            $scope.priceOriginal = angular.copy(data.price);
                            $scope.priceId = data.id;
                            $scope.editBan = data.editBan;
                            $scope.beginning = data.beginning;
                            $rootScope.branche = data.branche;
                            $scope.fBrancheName();
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                    });
        };

        // Удаляем прайс-лист с сервера
        $scope.removePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/remove', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка удаления! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Клонируем прайс-лист
        $scope.clonePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/clone', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Ошибка клонирования! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Подписываем прайс-лист
        $scope.signaturePrice = function () {

            if (!confirm("Редактировать и удалить подписанный\nпрайс-лист невозможно. Продолжить?")) {
                return false;
            }

            $scope.savePriceProcess = true;
            $http.post('/mods/price/signature', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        // Запускаем прайс-лист в работу
        $scope.beginningPrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/beginning', {priceId: $scope.priceId})
                    .success(function (data) {
                        if (data === false) {
                            alert('Произошла ошибка! Попробуйте еще раз.');
                            $scope.savePriceProcess = false;
                        }
                        if (data !== false) {
                            $scope.savePriceProcess = false;
                            $location.path('#/');
                        }
                    })
                    .error(function (data, status) {
                        alert('Сервер недоступен! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    });
        };

        //</editor-fold>

        if ($routeParams.id === undefined) {
            $scope.price = price;
            $scope.priceOriginal = angular.copy(price);
            $scope.priceId = 0;
            $scope.priceIsLoad = false;
            $scope.priceNomer = '';

            if ($rootScope.branche === undefined) {
                $location.path('#/');
            }

        } else {
            $scope.price = false;
            $scope.loadPrice($routeParams.id);
            $scope.priceIsLoad = true;
            $scope.priceNomer = '№' + $routeParams.id;
        }

    }]);
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="item4Ctrl - прайс-лист на алмазную резку">
app.controller('item4Ctrl', ['$rootScope', '$scope', '$http', '$location', '$routeParams', '$timeout', 'orderByFilter',
    function ($rootScope, $scope, $http, $location, $routeParams, $timeout, orderBy) {
        var DEFAULT_MIN_PRICE = 1500;

        // Права текущего пользователя
        $rootScope.access = mod_price_access;

        //<editor-fold defaultstate="collapsed" desc="Формируем начальный прайс-лист">

        $scope.savePriceProcess = false;

        var price = {
            groups: [
                {name: 'Асфальт', sum: 3000, min: 0},
                {name: 'Блок, кирпич', sum: 3750, min: 0},
                {name: 'Бетон, камень', sum: 5000, min: 0},
                {name: 'Армированный бетон', sum: 6250, min: 0}
            ],
            items: [
                {instrument: 'Штроборез с пылесосом', tip: '220 В', pros: 'только сухой', max: 120},
                {instrument: 'Бензорез', tip: 'Бензин', pros: 'сухой возможен / мокрый', max: 150},
                {instrument: 'Cut-n-breake', tip: '220 В', pros: 'сухой возможен / мокрый', max: 400},
                {instrument: 'Швонерезчик', tip: 'Бензин, дизель, 380 В', pros: 'только мокрый', max: 320},
                {instrument: 'Стенарезная машина', tip: '380 В', pros: 'сухой возможен / мокрый', max: 730},
                {instrument: 'Канатная машина', tip: '220 В / 380 В', pros: 'сухой возможен / мокрый', max: 4000}
            ],
            factors: [
                {code: 1, param: 0.2, name: "Повышенное содержание арматуры (более 150 кг/м3), толщина арматурного стержня 20 мм и более."},
                {code: 7.1, param: -0.05, name: "Скидка постоянному клиенту 5%"},
                {code: 2.1, param: 0.2, name: "Тяжелый, плотный бетон класса: B25"}
            ],
            result: [],
            params: {
                branche: 0,
                type: 2,
                round: 0,
                minPrice: DEFAULT_MIN_PRICE
            },
            comment: ''
        };

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="functions">

        // Удаляем ценовую группу
        $scope.delGroup = function (key) {
            if ($scope.price.groups.length === 1) {
                return;
            }
            $scope.price.groups.splice(key, 1);
        };

        // Добавляем ценовую группу
        $scope.addGroup = function () {
            var length = $scope.price.groups.length + 1;
            if (length > 5) {
                return false;
            }
            $scope.price.groups.push({
                name: 'Цена ' + length,
                sum: 0
            });
            return false;
        };

        // Удаляем прайсовую позицию
        $scope.delItem = function (key) {
            if ($scope.price.items.length === 1) {
                return;
            }
            $scope.price.items.splice(key, 1);
        };

        // Добавляем прайсовую позицию
        $scope.addItem = function () {
            $scope.price.items.push({
                instrument: "",
                tip: '',
                pros: '',
                max: 1
            });
            $scope.price.items = orderBy($scope.price.items, 'instrument', false);
            return false;
        };

        // Удаляем коэффициент
        $scope.delFactor = function (key) {
            if ($scope.price.factors.length === 1) {
                return;
            }
            $scope.price.factors.splice(key, 1);
        };

        // Добавляем коэффициент
        $scope.addFactor = function () {
            $scope.price.factors.push({
                code: 0,
                param: 0,
                name: '???'
            });
            $scope.price.factors = orderBy($scope.price.factors, 'code', false);
            return false;
        };

        // Высчитываем стоимость всех прайсовых позиций
        $scope.sumItems = function () {
            var oldResults = $scope.price.result;
            $scope.price.result = [];
            angular.forEach($scope.price.items, function (v, k) {
                $scope.sumItem(k);
            });
            $scope.price.items = orderBy($scope.price.items, 'instrument', false);
            $scope.price.factors = orderBy($scope.price.factors, 'code', false);
            applyConstValues();

            function applyConstValues() {
                angular.forEach($scope.price.result, function (v, k) {
                    var value = findByKey(oldResults, v['k']);

                    if (value && v['k'] === value['k'] && value['const']) {
                        v['const'] = true;
                        v['constValues'] = value['constValues'];
                    }
                });
            }

            function findByKey(arr, key) {
                return arr.find(function (value) {
                    return value['k'] === key;
                });
            }
        };

        // Узнаем наименование филиала по номеру
        $scope.brancheName = '';
        $scope.fBrancheName = function () {
            angular.forEach($rootScope.branche, function (v, k) {
                if (v.id === $scope.price.params.branche) {
                    $scope.brancheName = '(' + v.name + ')';
                }
            });
        };

        // Высчитываем стоимость прайсовой позиции
        $scope.sumItem = function (key) {
            var G2 = $scope.price.params.round;
            var L1 = $scope.price.items[key].instrument;
            var Result = [];
            angular.forEach($scope.price.groups, function (v, k) {
                var out = v['sum'];

                if (+G2 === 2) {
                    out = Math.ceil(out * 100) / 100;
                } else if (+G2 === 1) {
                    out = Math.ceil(out * 10) / 10;
                } else if (+G2 === 0) {
                    out = Math.ceil(out);
                }

                Result[k] = out < v['min'] ? v['min'] : out;
            });
            $scope.price.result.push({'k': L1, 'v': Result});
        };

        // Сохранение прайс-листа на сервере
        $scope.savePrice = function () {
            $scope.savePriceProcess = true;
            var priceToSave = applyConstValues();

            $http.post('/mods/price/item', {priceId: $scope.priceId, price: priceToSave})
                .success(function (data) {
                    if (data === false) {
                        alert('Ошибка сохранения! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    }
                    if (data !== false) {
                        $scope.savePriceProcess = false;
                        $location.path('#/');
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен! Попробуйте еще раз.');
                    $scope.savePriceProcess = false;
                });

            function applyConstValues() {
                var result = angular.copy($scope.price);

                angular.forEach(result.result, function (v, k) {
                    if (v['const']) {
                        v['v'] = v['constValues'];
                    } else {
                        delete  v['const'];
                        delete  v['constValues'];
                    }
                });

                return result;
            }
        };

        // Прайс был изменен?
        $scope.isEdit = function () {
            if ($scope.price === false) {
                return false;
            }
            if (
                angular.equals($scope.price.groups, $scope.priceOriginal.groups) &&
                angular.equals($scope.price.items, $scope.priceOriginal.items) &&
                angular.equals($scope.price.factors, $scope.priceOriginal.factors) &&
                angular.equals($scope.price.comment, $scope.priceOriginal.comment) &&
                angular.equals($scope.price.params, $scope.priceOriginal.params) &&
                isResultChanged()
            ) {
                return false;
            } else {
                return true;
            }

            function isResultChanged() {
                var result = true;

                angular.forEach($scope.price.result, function (v, k) {
                    if (!result) {
                        return;
                    }

                    var val1 = v['const']
                        ? v['constValues']
                        : v['v'];
                    var val2 = $scope.priceOriginal.result[k]['const']
                        ? $scope.priceOriginal.result[k]['constValues']
                        : $scope.priceOriginal.result[k]['v'];

                    result = angular.equals(val1, val2);
                });

                return result;
            }
        };

        // Получаем прайс-лист с сервера
        $scope.loadPrice = function (id) {
            $scope.priceIsLoad = true;
            $http.get('/mock/price/item_id=' + id + '.json')
                .success(function (data) {
                    if (data === false) {
                        alert('Произошла ошибка! Попробуйте еще раз.');
                    }
                    if (data !== false) {
                        $scope.priceIsLoad = false;
                        $scope.price = data.price;
                        if ($scope.price.result) {
                            $scope.price.params.minPrice = $scope.price.params.minPrice || DEFAULT_MIN_PRICE;
                        }
                        $scope.priceOriginal = angular.copy(data.price);
                        $scope.priceId = data.id;
                        $scope.editBan = data.editBan;
                        $scope.beginning = data.beginning;
                        $rootScope.branche = data.branche;
                        $scope.fBrancheName();
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен! Попробуйте еще раз.');
                });
        };

        // Удаляем прайс-лист с сервера
        $scope.removePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/remove', {priceId: $scope.priceId})
                .success(function (data) {
                    if (data === false) {
                        alert('Ошибка удаления! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    }
                    if (data !== false) {
                        $scope.savePriceProcess = false;
                        $location.path('#/');
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен! Попробуйте еще раз.');
                    $scope.savePriceProcess = false;
                });
        };

        // Клонируем прайс-лист
        $scope.clonePrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/clone', {priceId: $scope.priceId})
                .success(function (data) {
                    if (data === false) {
                        alert('Ошибка клонирования! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    }
                    if (data !== false) {
                        $scope.savePriceProcess = false;
                        $location.path('#/');
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен! Попробуйте еще раз.');
                    $scope.savePriceProcess = false;
                });
        };

        // Подписываем прайс-лист
        $scope.signaturePrice = function () {

            if (!confirm("Редактировать и удалить подписанный\nпрайс-лист невозможно. Продолжить?")) {
                return false;
            }

            $scope.savePriceProcess = true;
            $http.post('/mods/price/signature', {priceId: $scope.priceId})
                .success(function (data) {
                    if (data === false) {
                        alert('Произошла ошибка! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    }
                    if (data !== false) {
                        $scope.savePriceProcess = false;
                        $location.path('#/');
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен! Попробуйте еще раз.');
                    $scope.savePriceProcess = false;
                });
        };

        // Запускаем прайс-лист в работу
        $scope.beginningPrice = function () {
            $scope.savePriceProcess = true;
            $http.post('/mods/price/beginning', {priceId: $scope.priceId})
                .success(function (data) {
                    if (data === false) {
                        alert('Произошла ошибка! Попробуйте еще раз.');
                        $scope.savePriceProcess = false;
                    }
                    if (data !== false) {
                        $scope.savePriceProcess = false;
                        $location.path('#/');
                    }
                })
                .error(function (data, status) {
                    alert('Сервер недоступен! Попробуйте еще раз.');
                    $scope.savePriceProcess = false;
                });
        };

        // включаем\отключаем жестко забитую стоимость
        $scope.toggleConstCosts = function (value) {
            value['const'] = !value['const'];

            if (!value['constValues']) {
                value['constValues'] = angular.copy(value['v']);
            }

            if (value['const']) {
                angular.forEach($scope.price.groups, function (v, k) {
                    value['constValues'][k] = value['constValues'][k] || value['constValues'][k] === 0
                        ? value['constValues'][k]
                        : value['v'][k];
                });
            }
        };

        $scope.constValuesChanges = function (constValues, key) {
            var newValue = constValues[key] < 0
                ? Math.abs(constValues[key])
                : constValues[key] || 0;

            $timeout(function () {
                constValues[key] = newValue;
            });
        };
        //</editor-fold>

        if ($routeParams.id === undefined) {
            $scope.price = price;
            $scope.priceOriginal = angular.copy(price);
            $scope.priceId = 0;
            $scope.priceIsLoad = false;
            $scope.priceNomer = '';

            if ($rootScope.branche === undefined) {
                $location.path('#/');
            }

        } else {
            $scope.price = false;
            $scope.loadPrice($routeParams.id);
            $scope.priceIsLoad = true;
            $scope.priceNomer = '№' + $routeParams.id;
        }

    }]);
//</editor-fold>

